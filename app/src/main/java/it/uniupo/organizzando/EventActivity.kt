package it.uniupo.organizzando

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_event.*
import kotlinx.android.synthetic.main.price_review_title_section.*
import com.google.firebase.auth.*


class EventActivity : AppCompatActivity() {

    private var buttonaddserv: Button? = null
    private var buttonaddinv: Button? = null
    private var buttonmodify: Button? = null
    private var buttonmap: Button? = null
    private var buttonfeed: Button? = null
    private var imageURL: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event)
        val db = FirebaseFirestore.getInstance()
        buttonaddserv = findViewById<View>(R.id.btnaddserv) as Button
        buttonaddinv = findViewById<View>(R.id.btnaddinv) as Button
        buttonmodify = findViewById<View>(R.id.btnmodify2) as Button
        buttonmap = findViewById<View>(R.id.btnmapsguest) as Button
        buttonfeed = findViewById<View>(R.id.btnfeed) as Button
        val event = intent.getSerializableExtra("Event") as? Event
        if (event!=null){
            Log.e("Event", event.toString())
            loadViews(event)
        }

        val user = FirebaseAuth.getInstance().currentUser
        user?.let {
            val email = user.email.toString()
            val docRef = db.collection("utenti").document(email)
            docRef.get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        val ris = document.data?.get("tipo").toString()
                        Log.d(ContentValues.TAG, "DocumentSnapshot data: $ris")
                        if (ris == "3")
                        {
                            btnaddinv.visibility = View.GONE
                            btnaddserv.visibility = View.GONE
                            btnmodify2.visibility = View.GONE
                            budgetattuale.text = ""
                            budget.text=""
                        }else
                        {
                            btnmapsguest.visibility = View.GONE
                        }
                    } else {
                        Log.d(ContentValues.TAG, "No such document")
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d(ContentValues.TAG, "get failed with ", exception)
                }
        }


        buttonaddinv!!.setOnClickListener {


           val intent = Intent(this@EventActivity, AddguestActivity::class.java)

            intent.putExtra("Event", event)
            startActivity(intent)

        }

        buttonfeed!!.setOnClickListener {


            val intent = Intent(this@EventActivity, FeedActivity::class.java)

            intent.putExtra("Event", event)
            startActivity(intent)

        }

        buttonaddserv!!.setOnClickListener {


            val intent = Intent(this@EventActivity, ChooseserviceActivity::class.java)
              intent.putExtra("evento", event)
              startActivity(intent)

        }

        buttonmodify!!.setOnClickListener {

            val intent = Intent(this@EventActivity, ModevActivity::class.java)
            intent.putExtra("evento", event)
            startActivity(intent)

        }

        buttonmap!!.setOnClickListener {

            val intent = Intent(this@EventActivity, GuestmapActivity::class.java)
            intent.putExtra("evento", event)
            startActivity(intent)

        }

    }

    private fun loadViews(event: Event) {

        titoloevento.text = event.name
        data.text =   event.date
        ora.text = "Ora: "+event.hour
        luogo.text = "Luogo: "+event.place
        categoria.text = event.category
        budget.text = "Budget totale: ${event.budget}"
        proprietario.text = "Proprietario: "+event.owner
        budgetattuale.text = "Budget attuale: "+event.actualbudget.toString()

    }
}