package it.uniupo.organizzando

import android.content.ContentValues
import android.content.ContentValues.TAG
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import kotlinx.android.synthetic.main.activity_chooseservice2.*
import java.io.IOException
import java.text.DecimalFormat


class ChooseserviceActivity : AppCompatActivity() {
    private var auth: FirebaseAuth? = null
    private var adapter: ServiceAdapter2? = null

    private var filterButton: ImageView? = null
    private var filterView1: LinearLayout? = null
    private var filterView2: LinearLayout? = null
    private var filterView3: LinearLayout? = null
    private var filterMaps: LinearLayout? = null
    var filterHidden = true
    private var selectedFilter = "tutte"
    private var eventmain: Event? = null
    private var btnback: ImageView? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chooseservice2)

        auth = FirebaseAuth.getInstance()
        btnback = findViewById<View>(R.id.iconaback7) as ImageView

        initWidgets();
        hideFilter();


        val event = intent.getSerializableExtra("evento") as? Event

        if (event != null) {
            eventmain= event
            val stringa = "budget attuale: "+event.budget.toString()
            budgetchoose2.text=stringa

            val allServiceList: ArrayList<Service> = arrayListOf()

            allServiceList.clear()


            db().collection("servizi")
                .get()
                .addOnSuccessListener { documents ->
                    for (document in documents) {
                        allServiceList.add(Service(
                            document.data["nome"].toString(),
                            document.data["descrizione"].toString(),
                            document.data["immagine"].toString(),
                            document.data["prezzo"].toString().toLong(),
                            document.data["categoria"].toString(),
                            document.data["orario"].toString(),
                            document.data["proprietario"].toString(),
                            document.data["luogo"].toString(),
                            arrayListOf(document.data["giorni"].toString()),
                        ))
                    }


                    recyclerViewServizi3.layoutManager = GridLayoutManager(this, 2)
                    recyclerViewServizi3.adapter = ServiceAdapter2(services = allServiceList,event)
                }
                .addOnFailureListener { exception ->
                    Log.w(TAG, "Error getting documents: ", exception)
                }


        }

        btnback!!.setOnClickListener(View.OnClickListener {
            startIntent(OrganizzatoremainActivity::class.java)
        })


    }


    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }

    fun db(): FirebaseFirestore {
        return FirebaseFirestore.getInstance()
    }

    fun showFilterTapped(view: View?) {
        if (filterHidden) {
            filterHidden = false
            showFilter()
        } else {
            filterHidden = true
            hideFilter()
        }
    }


    private fun initWidgets() {
        filterButton = findViewById<View>(R.id.filterButton) as ImageView
        filterView1 = findViewById<View>(R.id.filterTabsLayout) as LinearLayout
        filterView2 = findViewById<View>(R.id.filterTabsLayout2) as LinearLayout
        filterView3 = findViewById<View>(R.id.filterTabsLayout3) as LinearLayout
        filterMaps = findViewById<View>(R.id.filterMaps) as LinearLayout
    }

    private fun hideFilter() {
        filterView1!!.setVisibility(View.GONE)
        filterView2!!.setVisibility(View.GONE)
        filterView3!!.setVisibility(View.GONE)
        filterMaps!!.setVisibility(View.GONE)
    }

    private fun showFilter() {
        filterView1!!.setVisibility(View.VISIBLE)
        filterView2!!.setVisibility(View.VISIBLE)
        filterView3!!.setVisibility(View.VISIBLE)
        filterMaps!!.setVisibility(View.VISIBLE)

    }



    fun allFilterTapped(view: View?) {
        selectedFilter = "tutte"

        val lista: ArrayList<Service> = arrayListOf()

        lista.clear()

        db().collection("servizi")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    lista.add(Service(
                        document.data["nome"].toString(),
                        document.data["descrizione"].toString(),
                        document.data["immagine"].toString(),
                        document.data["prezzo"].toString().toLong(),
                        document.data["categoria"].toString(),
                        document.data["orario"].toString(),
                        document.data["proprietario"].toString(),
                        document.data["luogo"].toString(),
                        arrayListOf(document.data["giorni"].toString()),
                    ))
                }

                setAdapter(lista)

            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }

    }

    fun photoFilterTapped(view: View) {
        selectedFilter = "Fotografia"

        val lista: ArrayList<Service> = arrayListOf()

        lista.clear()

        db().collection("servizi")
            .whereEqualTo("categoria", "Fotografia")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    lista.add(Service(
                        document.data["nome"].toString(),
                        document.data["descrizione"].toString(),
                        document.data["immagine"].toString(),
                        document.data["prezzo"].toString().toLong(),
                        document.data["categoria"].toString(),
                        document.data["orario"].toString(),
                        document.data["proprietario"].toString(),
                        document.data["luogo"].toString(),
                        arrayListOf(document.data["giorni"].toString()),
                    ))
                }

                setAdapter(lista)

            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }
    }

    fun musicFilterTapped(view: View) {
        selectedFilter = "Musica"

        val lista: ArrayList<Service> = arrayListOf()

        lista.clear()

        db().collection("servizi")
            .whereEqualTo("categoria", "Musica")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    lista.add(Service(
                        document.data["nome"].toString(),
                        document.data["descrizione"].toString(),
                        document.data["immagine"].toString(),
                        document.data["prezzo"].toString().toLong(),
                        document.data["categoria"].toString(),
                        document.data["orario"].toString(),
                        document.data["proprietario"].toString(),
                        document.data["luogo"].toString(),
                        arrayListOf(document.data["giorni"].toString())
                    ))
                }

                setAdapter(lista)

            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }
    }
    fun flowerFilterTapped(view: View) {
        selectedFilter = "Fioraio"

        val lista: ArrayList<Service> = arrayListOf()

        lista.clear()

        db().collection("servizi")
            .whereEqualTo("categoria", "Fioraio")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    lista.add(Service(
                        document.data["nome"].toString(),
                        document.data["descrizione"].toString(),
                        document.data["immagine"].toString(),
                        document.data["prezzo"].toString().toLong(),
                        document.data["categoria"].toString(),
                        document.data["orario"].toString(),
                        document.data["proprietario"].toString(),
                        document.data["luogo"].toString(),
                        arrayListOf(document.data["giorni"].toString())
                    ))
                }

                setAdapter(lista)

            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }
    }
    fun transportFilterTapped(view: View) {
        selectedFilter = "Trasporto"

        val lista: ArrayList<Service> = arrayListOf()

        lista.clear()

        db().collection("servizi")
            .whereEqualTo("categoria", "Trasporto")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    lista.add(Service(
                        document.data["nome"].toString(),
                        document.data["descrizione"].toString(),
                        document.data["immagine"].toString(),
                        document.data["prezzo"].toString().toLong(),
                        document.data["categoria"].toString(),
                        document.data["orario"].toString(),
                        document.data["proprietario"].toString(),
                        document.data["luogo"].toString(),
                        arrayListOf(document.data["giorni"].toString())
                    ))
                }

                setAdapter(lista)

            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }
    }
    fun hairFilterTapped(view: View) {
        selectedFilter = "Parrucchiere"

        val lista: ArrayList<Service> = arrayListOf()

        lista.clear()

        db().collection("servizi")
            .whereEqualTo("categoria", "Parrucchiere")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    lista.add(Service(
                        document.data["nome"].toString(),
                        document.data["descrizione"].toString(),
                        document.data["immagine"].toString(),
                        document.data["prezzo"].toString().toLong(),
                        document.data["categoria"].toString(),
                        document.data["orario"].toString(),
                        document.data["proprietario"].toString(),
                        document.data["luogo"].toString(),
                        arrayListOf(document.data["giorni"].toString())
                    ))
                }

                setAdapter(lista)

            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }
    }
    fun cateringFilterTapped(view: View) {
        selectedFilter = "Catering"

        val lista: ArrayList<Service> = arrayListOf()

        lista.clear()

        db().collection("servizi")
            .whereEqualTo("categoria", "Catering")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    lista.add(Service(
                        document.data["nome"].toString(),
                        document.data["descrizione"].toString(),
                        document.data["immagine"].toString(),
                        document.data["prezzo"].toString().toLong(),
                        document.data["categoria"].toString(),
                        document.data["orario"].toString(),
                        document.data["proprietario"].toString(),
                        document.data["luogo"].toString(),
                        arrayListOf(document.data["giorni"].toString())
                    ))
                }

                setAdapter(lista)

            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }
    }
    fun securityFilterTapped(view: View) {
        selectedFilter = "Security"

        val lista: ArrayList<Service> = arrayListOf()

        lista.clear()

        db().collection("servizi")
            .whereEqualTo("categoria", "Security")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    lista.add(Service(
                        document.data["nome"].toString(),
                        document.data["descrizione"].toString(),
                        document.data["immagine"].toString(),
                        document.data["prezzo"].toString().toLong(),
                        document.data["categoria"].toString(),
                        document.data["orario"].toString(),
                        document.data["proprietario"].toString(),
                        document.data["luogo"].toString(),
                        arrayListOf(document.data["giorni"].toString())
                    ))
                }

                setAdapter(lista)

            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }
    }
    fun otherFilterTapped(view: View) {
        selectedFilter = "Altro"

        val lista: ArrayList<Service> = arrayListOf()

        lista.clear()

        db().collection("servizi")
            .whereEqualTo("categoria", "Altro")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    lista.add(Service(
                        document.data["nome"].toString(),
                        document.data["descrizione"].toString(),
                        document.data["immagine"].toString(),
                        document.data["prezzo"].toString().toLong(),
                        document.data["categoria"].toString(),
                        document.data["orario"].toString(),
                        document.data["proprietario"].toString(),
                        document.data["luogo"].toString(),
                        arrayListOf(document.data["giorni"].toString())
                    ))
                }

                setAdapter(lista)

            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents: ", exception)
            }
    }

    private fun setAdapter(serviceList: ArrayList<Service>) {
        val adapter = ServiceAdapter2(services = serviceList, eventmain!!)
        recyclerViewServizi3.adapter = adapter
    }

    fun CalculationByDistance(StartP: LatLng, EndP: LatLng): Double {
        val Radius = 6371 // radius of earth in Km
        val lat1 = StartP.latitude
        val lat2 = EndP.latitude
        val lon1 = StartP.longitude
        val lon2 = EndP.longitude
        val dLat = Math.toRadians(lat2 - lat1)
        val dLon = Math.toRadians(lon2 - lon1)
        val a = (Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + (Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2)))
        val c = 2 * Math.asin(Math.sqrt(a))
        val valueResult = Radius * c
        val km = valueResult / 1
        val newFormat = DecimalFormat("####")
        val kmInDec: Int = Integer.valueOf(newFormat.format(km))
        val meter = valueResult % 1000
        val meterInDec: Int = Integer.valueOf(newFormat.format(meter))
        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec)
        return Radius * c
    }

    fun getLocationFromAddress(strAddress: String?): LatLng? {
        val coder = Geocoder(this)
        val address: List<Address>?
        var p1: GeoPoint? = null
        try {
            address = coder.getFromLocationName(strAddress, 5)
            if (address == null) {
                return null
            }
            val location: Address = address[0]
            location.getLatitude()
            location.getLongitude()
            p1 = GeoPoint(
                (location.getLatitude() ) as Double,
                (location.getLongitude()) as Double
            )
            val lat: Double = p1.getLatitude()
            val lng: Double = p1.getLongitude()
            val latLng = LatLng(lat, lng)
            return latLng
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }

    fun nearFilterTapped(view: View) {
        if(distance.text.isEmpty()){
            Toast.makeText(this, "Inserisci una distanza valida", Toast.LENGTH_SHORT).show()


        }else{
            selectedFilter = "Vicini"

            var d1= getLocationFromAddress(eventmain!!.place)


            var lista2: ArrayList<Service> = getAllService()

            lista2.clear()

            val lista: ArrayList<Service> = arrayListOf()

            lista.clear()

            db().collection("servizi")
                .get()
                .addOnSuccessListener { documents ->
                    for (document in documents) {
                        lista.add(Service(
                            document.data["nome"].toString(),
                            document.data["descrizione"].toString(),
                            document.data["immagine"].toString(),
                            document.data["prezzo"].toString().toLong(),
                            document.data["categoria"].toString(),
                            document.data["orario"].toString(),
                            document.data["proprietario"].toString(),
                            document.data["luogo"].toString(),
                            arrayListOf(document.data["giorni"].toString())
                        ))
                    }
                    lista2=lista
                    var d2: LatLng? = null
                    for(s in lista2)
                    {
                        d2=getLocationFromAddress(s.place)
                        if(CalculationByDistance(d1!!,d2!!)>distance.text.toString().toInt())
                        {
                            lista2.remove(s)
                        }
                    }
                    setAdapter(lista2)
                }
                .addOnFailureListener { exception ->
                    Log.w(ContentValues.TAG, "Error getting documents: ", exception)
                }
        }
    }

    fun startmap(view: View) {
        val intent = Intent(this@ChooseserviceActivity, ServicemarkerActivity::class.java)
        intent.putExtra("evento", eventmain)
        startActivity(intent)
    }

    override fun onBackPressed() {
        startActivity(Intent(this, OrganizzatoremainActivity::class.java))
    }
}