package it.uniupo.organizzando

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_fornitoretrue.*
import kotlinx.android.synthetic.main.activity_organizzatoretrue.*

class FornitoretruemainActivity : AppCompatActivity() {
    private var btnservice: Button? = null
    private var btnoption: Button? = null
    private var btnquote: Button? = null
    private var btnlogout: Button? = null
    private var btnmeeting: Button? = null
    private var auth: FirebaseAuth? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fornitoretrue)


        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance()
        btnservice = findViewById<View>(R.id.btn_myservice) as Button
        btnoption = findViewById<View>(R.id.btn_myoptionforn) as Button
        btnquote = findViewById<View>(R.id.btn_myquoteforn) as Button
        btnlogout = findViewById<View>(R.id.btn_mylogoutforn) as Button
        btnmeeting = findViewById<View>(R.id.btn_mymeeting) as Button

        val user = FirebaseAuth.getInstance().currentUser?.email

        val stringa = "benvenuto $user!"

        titletrueforn.text= stringa

        btnservice!!.setOnClickListener {
            startIntent(FornitoremainActivity::class.java)
        }

        btnoption!!.setOnClickListener {
            startIntent(OptionsAccountActivity::class.java)
        }

        btnquote!!.setOnClickListener {
            startIntent(QuotelistActivity::class.java)
        }

        btnmeeting!!.setOnClickListener {
            startIntent(MeetinglistActivity::class.java)
        }

        btnlogout!!.setOnClickListener {
            signOut()
            startIntent(LoginActivity::class.java)
        }
    }

    private fun signOut() {
        auth!!.signOut()
    }


    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }
}