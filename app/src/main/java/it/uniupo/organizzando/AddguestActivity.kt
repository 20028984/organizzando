package it.uniupo.organizzando

import android.content.*
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_addguest.*
import kotlinx.android.synthetic.main.price_review_title_section.*
import com.google.firebase.auth.*


class AddguestActivity: AppCompatActivity() {

    private var buttonshare: Button? = null
    private var buttoncopy: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addguest)
        val db = FirebaseFirestore.getInstance()
        buttonshare = findViewById<View>(R.id.btnshare) as Button
        buttoncopy = findViewById<View>(R.id.btncopy) as ImageView
        val event = intent.getSerializableExtra("Event") as? Event

        val stringa = "Ecco il tuo codice invito: ${event!!.invitecode}"

        textinvito.text= stringa

        buttoncopy!!.setOnClickListener{
            val textToCopy = event.invitecode
            val clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clipData = ClipData.newPlainText("text", textToCopy)
            clipboardManager.setPrimaryClip(clipData)
            Toast.makeText(this, "Testo copiato negli appunti!", Toast.LENGTH_LONG).show()
        }

        buttonshare!!.setOnClickListener {

            sendEmail("Sei ufficialmente invitato all'evento ${event.name}! Ecco il tuo codice invito: ${event.invitecode}")
        }

    }

    private fun sendEmail(message: String) {
        /*ACTION_SEND action to launch an email client installed on your Android device.*/
        val mIntent = Intent(Intent.ACTION_SEND)
        /*To send an email you need to specify mailto: as URI using setData() method
        and data type will be to text/plain using setType() method*/
        mIntent.data = Uri.parse("mailto:")
        mIntent.type = "text/plain"
        // put recipient email in intent
        /* recipient is put as array because you may wanna send email to multiple emails
           so enter comma(,) separated emails, it will be stored in array*/
        //put the message in the intent
        mIntent.putExtra(Intent.EXTRA_TEXT, message)


        try {
            //start email intent
            startActivity(Intent.createChooser(mIntent, "Choose Email Client..."))
        }
        catch (e: Exception){
            //if any thing goes wrong for example no email client application or any exception
            //get and show exception message
            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
        }

    }


}