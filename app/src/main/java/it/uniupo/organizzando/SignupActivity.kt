package it.uniupo.organizzando

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore



class SignupActivity : AppCompatActivity() {
    private var inputEmail: EditText? = null
    private var inputPassword: EditText? = null
    private var btnSignIn: Button? = null
    private var btnSignUp: Button? = null
    private var btnResetPassword: Button? = null
    private var progressBar: ProgressBar? = null
    private var auth: FirebaseAuth? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        val db = FirebaseFirestore.getInstance()
        val TAG = SignupActivity::class.java.simpleName



        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance()
        btnSignIn = findViewById<View>(R.id.sign_in_button) as Button
        btnSignUp = findViewById<View>(R.id.sign_up_button) as Button
        inputEmail = findViewById<View>(R.id.email) as EditText
        inputPassword = findViewById<View>(R.id.password) as EditText
        progressBar = findViewById<View>(R.id.progressBar) as ProgressBar
        val spinner: Spinner = findViewById(R.id.spinnerutente)
        btnResetPassword = findViewById<View>(R.id.btn_reset_password) as Button
        btnResetPassword!!.setOnClickListener {
            startActivity(
                Intent(
                    this@SignupActivity,
                    ResetPasswordActivity::class.java
                )
            )
        }
        btnSignIn!!.setOnClickListener { finish() }
        btnSignUp!!.setOnClickListener(View.OnClickListener {
            val email = inputEmail!!.text.toString().trim { it <= ' ' }
            val password = inputPassword!!.text.toString().trim { it <= ' ' }
            val entry: MutableMap<String, Any> = HashMap()
            if (TextUtils.isEmpty(email)) {
                Toast.makeText(applicationContext, "Inserisci una email valida!", Toast.LENGTH_SHORT)
                    .show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(password)) {
                Toast.makeText(applicationContext, "Inserisci una password!", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (password.length < 6) {
                Toast.makeText(
                    applicationContext,
                    "Password troppo corta, inserisci minimo 6 caratteri!",
                    Toast.LENGTH_SHORT
                ).show()
                return@OnClickListener
            }
            val text: String = spinner.selectedItem.toString()
            val tipo: String
            if(text=="Fornitore"){
                entry["tipo"] = 1
                tipo="1"
            }
            else if (text=="Organizzatore") {
                entry["tipo"] = 2
                tipo="2"
            } else{
                entry["tipo"]= 3
                tipo="3"
            }
            progressBar!!.visibility = View.VISIBLE

            if(tipo=="3"){
            //create user
            auth!!.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(
                    this@SignupActivity
                ) { task ->
                    Toast.makeText(
                        this@SignupActivity,
                        "Utente creato con successo!",
                        Toast.LENGTH_SHORT
                    ).show()
                    progressBar!!.visibility = View.GONE
                    // If sign in fails, display a message to the user. If sign in succeeds
                    // the auth state listener will be notified and logic to handle the
                    // signed in user can be handled in the listener.
                    if (!task.isSuccessful) {
                        Toast.makeText(
                            this@SignupActivity, "Authentication failed." + task.exception,
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        val utente = hashMapOf(
                            "tipo" to 3,
                            "notifiche" to "tutte",
                            "nuovipost" to "si",
                            "mieicommenti" to "si",
                            "postcommentati" to "si"
                        )
                        db.collection("utenti").document(email)
                            .set(utente)
                            .addOnSuccessListener { documentReference ->
                                Log.d(TAG, "Document successful written!")
                            }
                            .addOnFailureListener { e ->
                                Log.w(TAG, "Error adding document", e)
                            }

                        startIntent(InvitatomainActivity::class.java)
                        finish()
                    }
                }
            }
            else if (tipo=="1"){
                    val intent = Intent(this@SignupActivity, FornitoreregActivity::class.java)
                    intent.putExtra("email", email)
                    intent.putExtra("password", password)
                    startActivity(intent)

            }else{
                val intent = Intent(this@SignupActivity, OrganizzatoreregActivity::class.java)
                intent.putExtra("email", email)
                intent.putExtra("password", password)
                startActivity(intent)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        progressBar!!.visibility = View.GONE
    }

    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }
}