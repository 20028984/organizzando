package it.uniupo.organizzando.Adapter

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import it.uniupo.organizzando.AddCommentActivity
import it.uniupo.organizzando.Model.Post
import it.uniupo.organizzando.R
import it.uniupo.organizzando.ShowUsersActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import it.uniupo.organizzando.FCMMessages
import kotlinx.android.synthetic.main.activity_add_post.*
import kotlinx.android.synthetic.main.activity_event.*


class PostAdapter
    (private val mContext:Context,private  val mPost:List<Post>):RecyclerView.Adapter<PostAdapter.ViewHolder>()
{
    private var firebaseUser:FirebaseUser?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view=LayoutInflater.from(mContext).inflate(R.layout.posts_layout,parent,false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
       return  mPost.size
    }

    //code for events
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        firebaseUser= FirebaseAuth.getInstance().currentUser
        val post=mPost[position]
        val postid=post.getPostId()
        val event= post.getEvent()
        val publisher = post.getPublisher()
        val postimage= post.getPostImage()

        Picasso.get().load(postimage).into(holder.postImage)
        holder.caption.text=post.getCaption()
        publisherInfo(holder.username,holder.publisher,post.getPublisher())
        isLiked(post.getPostId(),holder.likeButton,holder.postImage)
        getCountofLikes(post.getPostId(),holder.likes)
        getComments(post.getPostId(),holder.comments)



        holder.postImage.setOnClickListener {
            if (holder.postImage.tag.toString() == "like") {
                FirebaseDatabase.getInstance("https://organizzando-default-rtdb.europe-west1.firebasedatabase.app").reference.child("Likes").child(post.getPostId())
                    .child(firebaseUser!!.email!!.split(".")[0])
                    .setValue(true)
            } else {
                FirebaseDatabase.getInstance("https://organizzando-default-rtdb.europe-west1.firebasedatabase.app").reference.child("Likes").child(post.getPostId())
                    .child(firebaseUser!!.email!!.split(".")[0])
                    .removeValue()
            }

        }




        holder.likeButton.setOnClickListener{
            if (holder.likeButton.tag.toString()=="like")
            {
                val db = FirebaseFirestore.getInstance()
                val user = FirebaseAuth.getInstance().currentUser?.email
                val docRef = db.collection("utenti").document(post.getPostId())
                docRef.get()
                    .addOnSuccessListener { document ->
                        if (document != null) {
                            val ris = document.data?.get("token").toString()
                            val user_token = ris
                            val notification_title = "Nuovo Like"
                            val notification_des = "$user ha messo like al tuo post"
                            FCMMessages().sendMessageSingle(
                                mContext,
                                user_token,
                                notification_title,
                                notification_des,
                                null
                            )

                        } else {
                            Log.d(ContentValues.TAG, "No such document")
                        }
                    }
                    .addOnFailureListener { exception ->
                        Log.d(ContentValues.TAG, "get failed with ", exception)
                    }
                FirebaseDatabase.getInstance("https://organizzando-default-rtdb.europe-west1.firebasedatabase.app").reference.child("Likes").child(post.getPostId())
                    .child(firebaseUser!!.email!!.split(".")[0])
                    .setValue(true)
                pushNotification(post.getPostId(),post.getPublisher())
            }
            else
            {
                FirebaseDatabase.getInstance("https://organizzando-default-rtdb.europe-west1.firebasedatabase.app").reference.child("Likes").child(post.getPostId())
                    .child(firebaseUser!!.email!!.split(".")[0])
                    .removeValue()
            }
        }

        holder.comments.setOnClickListener {

            val intent = Intent(mContext,AddCommentActivity::class.java).apply {
                putExtra("POST_ID",postid)
                putExtra("Event",event)
                putExtra("Publisher",publisher)
                putExtra("Image",postimage)
            }
            mContext.startActivity(intent)
        }

        holder.likes.setOnClickListener {
            val intent = Intent(mContext, ShowUsersActivity::class.java)
            intent.putExtra("id",post.getPostId())
            intent.putExtra("title","likes")
            mContext.startActivity(intent)
        }

        holder.commentButton.setOnClickListener {

            val intent = Intent(mContext,AddCommentActivity::class.java).apply {
                putExtra("POST_ID",postid)
                putExtra("Event",event)
                putExtra("Publisher",publisher)
                putExtra("Image",postimage)
            }
            mContext.startActivity(intent)
        }


    }

    inner class ViewHolder(@NonNull itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        var postImage:ImageView
        var likeButton:ImageView
        var commentButton:ImageView
        var likes:TextView
        var comments:TextView
        var username:TextView
        var publisher:TextView
        var caption:TextView


        init {
            postImage=itemView.findViewById(R.id.post_image_home)
            likeButton=itemView.findViewById(R.id.post_image_like_btn)
            commentButton=itemView.findViewById(R.id.post_image_comment_btn)
            likes=itemView.findViewById(R.id.likes)
            comments=itemView.findViewById(R.id.comments)
            username=itemView.findViewById(R.id.publisher_user_name_post)
            publisher=itemView.findViewById(R.id.publisher)
            caption=itemView.findViewById(R.id.caption)

        }

    }

    private fun getComments(postid:String, comment:TextView) {

        val commentRef=FirebaseDatabase.getInstance("https://organizzando-default-rtdb.europe-west1.firebasedatabase.app").reference.child("Comment").child(postid)

        commentRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
            }

            override fun onDataChange(datasnapshot: DataSnapshot) {
                comment.text = "View all "+datasnapshot.childrenCount.toString()+" comments"
            }
        })
    }

    private fun pushNotification(postid:String, userid:String) {

        val ref = FirebaseDatabase.getInstance("https://organizzando-default-rtdb.europe-west1.firebasedatabase.app").reference.child("Notification").child(userid)

        val notifyMap = HashMap<String, Any>()
        notifyMap["userid"] = FirebaseAuth.getInstance().currentUser!!.uid
        notifyMap["text"] = "♥liked your post♥"
        notifyMap["postid"] = postid
        notifyMap["ispost"] = true

        ref.push().setValue(notifyMap)
    }


    private fun isLiked(postid:String,imageView: ImageView,postedImg:ImageView) {

        firebaseUser=FirebaseAuth.getInstance().currentUser
        val postRef=FirebaseDatabase.getInstance("https://organizzando-default-rtdb.europe-west1.firebasedatabase.app").reference.child("Likes").child(postid)

        postRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(datasnapshot: DataSnapshot) {
                if (datasnapshot.child(firebaseUser!!.email!!.split(".")[0]).exists()) {
                    imageView.setImageResource(R.drawable.heart_clicked)
                    postedImg.tag =" liked"
                    imageView.tag = "liked"
                }
                else {
                    imageView.setImageResource(R.drawable.heart_not_clicked)
                    postedImg.tag = "like"
                    imageView.tag = "like"
                }
            }
        })
    }

    private fun getCountofLikes(postid:String,likesNo: TextView) {

        val postRef=FirebaseDatabase.getInstance("https://organizzando-default-rtdb.europe-west1.firebasedatabase.app").reference.child("Likes").child(postid)

        postRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                }

            override fun onDataChange(datasnapshot: DataSnapshot) {
                likesNo.text = datasnapshot.childrenCount.toString()+" likes"
            }
        })
    }


    private fun publisherInfo(username: TextView, publisher: TextView, publisheremail: String) {

        val db = FirebaseFirestore.getInstance()

        val docRef = db.collection("utenti").document(publisheremail)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                   username.text= publisheremail
                    publisher.text= publisheremail
                } else {
                    Log.d(ContentValues.TAG, "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d(ContentValues.TAG, "get failed with ", exception)
            }
    }

}