package it.uniupo.organizzando.Adapter

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import it.uniupo.organizzando.Model.Comment
import it.uniupo.organizzando.Model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso
import it.uniupo.organizzando.FeedActivity
import it.uniupo.organizzando.R

class CommentAdapter(private var mContext: Context,
                     private var mComment:List<Comment>): RecyclerView.Adapter<CommentAdapter.ViewHolder>() {

    private var firebaseUser: FirebaseUser? = null

    inner class ViewHolder(@NonNull itemView: View) : RecyclerView.ViewHolder(itemView) {
        var publisher: TextView
        var publisher_comment: TextView


        init {
            publisher = itemView.findViewById(R.id.publisher_username)
            publisher_comment = itemView.findViewById(R.id.publisher_caption)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.comment_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mComment.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        firebaseUser = FirebaseAuth.getInstance().currentUser
        val comment = mComment[position]

        if(comment.getComment()!="")
        holder.publisher_comment.text=(comment.getComment())

        publisherInfo(holder.publisher, comment.getPublisher())

        holder.publisher.setOnClickListener {

                val intent = Intent(mContext, FeedActivity::class.java).apply {
                    putExtra("PUBLISHER_ID", comment.getPublisher())
                }
                mContext.startActivity(intent)
        }


    }

    private fun publisherInfo( username: TextView, publisheremail: String) {

       /* val userRef= FirebaseDatabase.getInstance("https://organizzando-default-rtdb.europe-west1.firebasedatabase.app").reference.child("Users").child(publisherID)
        userRef.addValueEventListener(object : ValueEventListener
        {
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists())
                {
                    val user = snapshot.getValue<User>(User::class.java)

                    Picasso.get().load(user!!.getImage()).placeholder(R.drawable.profile).into(profileImage)
                    username.text =(user.getUsername())
                }
            }

        })*/

        val db = FirebaseFirestore.getInstance()

        val docRef = db.collection("utenti").document(publisheremail)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    username.text= publisheremail
                } else {
                    Log.d(ContentValues.TAG, "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d(ContentValues.TAG, "get failed with ", exception)
            }
    }
}