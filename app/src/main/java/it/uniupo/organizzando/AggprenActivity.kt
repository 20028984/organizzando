package it.uniupo.organizzando

import android.content.ContentValues
import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import it.uniupo.organizzando.fragment.DatePickerFragment
import it.uniupo.organizzando.fragment.TimePickerFragment3
import kotlinx.android.synthetic.main.activity_aggpren.*
import org.json.JSONObject
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*


class AggprenActivity : AppCompatActivity() {

    private var buttonprenota: Button? = null
    private var data: TextView? = null
    private var backbutton: ImageView? = null
    private var ora: TextView? = null
    private var datamod: Boolean = false
    private var oramod: Boolean = false
    var eventmain: Event?= null
    private val FCM_API = "https://fcm.googleapis.com/fcm/send"
    private val serverKey =
        "key=" + "AAAAXFUnA0U:APA91bHVgmJCxeOC2chTqXoXLrRDxkf_Y3TooGz-CW6_za_JTBoW4TJAyKfQ2vrHJIoZzYFKcLAIh2RFzhStKv7HuUWKKB5IgmwasRJoHEYkzu3hkrrC_pAQvg9BZo2NnRkypR94uQI0"
    private val contentType = "application/json"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_aggpren)
        val db = FirebaseFirestore.getInstance()
        buttonprenota = findViewById<View>(R.id.buttonprenota2) as Button
        data = findViewById<View>(R.id.textView8) as TextView
        backbutton = findViewById<View>(R.id.iconaback8) as ImageView
        ora = findViewById<View>(R.id.textView9) as TextView
        val event = intent.getSerializableExtra("Event") as? Event
        eventmain= event
        val service = intent.getSerializableExtra("servizio") as? Service



        data!!.setOnClickListener{
           DatePickerFragment().show(supportFragmentManager, "timePicker")
            datamod=true
        }

        ora!!.setOnClickListener{
            TimePickerFragment3().show(supportFragmentManager, "timePicker")
            oramod=true
        }

        backbutton!!.setOnClickListener {
            val intent= Intent(this, ChooseserviceActivity::class.java)
            intent.putExtra("evento", eventmain)
            startActivity(intent)
        }


        buttonprenota!!.setOnClickListener {

            if(datamod && oramod ) {

                val sdf = SimpleDateFormat("EEEE")
                val stringhedata = textView8.text.toString().split(" ")
                val date1: Date = SimpleDateFormat("dd/MM/yyyy").parse(stringhedata[1]) as Date
                val stringheora = textView9.text.toString().split(" ")

                var orari= service!!.opening.split("-")
                val date2: Date = SimpleDateFormat("HH:mm:ss").parse(stringheora[1]+":00") as Date
                val dayOfTheWeek: String = sdf.format(date1)
                Log.d(ContentValues.TAG, "Giorno della settimana: $dayOfTheWeek")
                val cal = Calendar.getInstance() //Create Calendar-Object

                cal.time = date2 //Set the Calendar to now

                val hour = cal[Calendar.HOUR_OF_DAY] //Get the hour from the calendar

                val orafine = orari[1].split(":")
                val orainizio = orari[0].split(":")


                var arraygiorni = service.days.toString()
                arraygiorni= arraygiorni.replace("[","")
                arraygiorni= arraygiorni.replace("]","")
                arraygiorni= arraygiorni.replace(" ","")
                val listagiorni= arraygiorni.split(",")
                if(listagiorni.contains(dayOfTheWeek) && hour <= orafine[0].toInt() && hour >= orainizio[0].toInt())
                {
                    val extras = intent.extras
                    val nome = extras?.getString("nome");
                    val prop = extras?.getString("proprietario");
                    val user = FirebaseAuth.getInstance().currentUser?.email


                    val builder = AlertDialog.Builder(this, R.style.AlertDialog)
                    builder.setTitle("Conferma: ")
                    builder.setMessage("Sei sicuro di voler prenotare il servizio?")
                    builder.setPositiveButton(android.R.string.yes) { dialog, which ->



                        val stamp = Timestamp(System.currentTimeMillis()).toString()

                        var stringa =  extraapp.text.toString()

                        if(extraapp.text.isEmpty())
                        {
                            stringa= "nessuno"
                        }
                        val item = Meeting(
                            stamp,
                            nome!!,
                            user!!.toString(),
                            prop!!,
                            textView8.text.toString(),
                            textView9.text.toString(),
                            "in attesa di approvazione",
                            stringa,
                            event!!.name
                        )
                        val appuntamento = hashMapOf(
                            "codice" to stamp,
                            "nomeserv" to nome,
                            "organizzatore" to user.toString(),
                            "fornitore" to prop,
                            "data" to   textView8.text.toString().split("Data: ")[1],
                            "ora" to textView9.text.toString().split("Orario: ")[1],
                            "stato" to "in attesa di approvazione",
                            "extra" to stringa,
                            "evento" to event.name
                        )

                        db().collection("appuntamenti").document(stamp)
                            .set(appuntamento)
                            .addOnSuccessListener { Log.d(ContentValues.TAG, "DocumentSnapshot successfully written!") }
                            .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }

                        addMeeting(item)

                        val docRef = db.collection("utenti").document(prop)
                        docRef.get()
                            .addOnSuccessListener { document ->
                                if (document != null) {
                                    val ris = document.data?.get("token").toString()
                                    val user_token = ris
                                    val notification_title = "Nuovo appuntamento"
                                    val notification_des = "$user ha creato un nuovo appuntamento con te per il servizio $nome"
                                    FCMMessages().sendMessageSingle(
                                        this@AggprenActivity,
                                        user_token,
                                        notification_title,
                                        notification_des,
                                        null
                                    )

                                } else {
                                    Log.d(TAG, "No such document")
                                }
                            }
                            .addOnFailureListener { exception ->
                                Log.d(TAG, "get failed with ", exception)
                            }




                        Toast.makeText(
                            applicationContext,
                            "Appuntamento creato!", Toast.LENGTH_SHORT
                        ).show()
                        val intent = Intent(this@AggprenActivity, MeetinglistActivity::class.java)
                        startActivity(intent)
                    }

                    builder.setNegativeButton("No") { dialog, which ->
                        Toast.makeText(
                            applicationContext,
                            "operazione annullata", Toast.LENGTH_SHORT
                        ).show()
                    }
                    builder.show()

                }else
                {
                    Toast.makeText(this, "Orario o data inseriti inconpatibili con il servizio!", Toast.LENGTH_SHORT).show()
                }



            }else
            {
                Toast.makeText(
                    applicationContext,
                    "Devi prima selezionare la data e l'ora!", Toast.LENGTH_SHORT
                ).show()
            }

        }


    }

    fun db(): FirebaseFirestore {
        return FirebaseFirestore.getInstance()
    }

    private val requestQueue: RequestQueue by lazy {
        Volley.newRequestQueue(this.applicationContext)
    }

    private fun sendNotification(notification: JSONObject) {
        Log.e("TAG", "sendNotification")
        val jsonObjectRequest = object : JsonObjectRequest(FCM_API, notification,
            Response.Listener<JSONObject> { response ->
                Log.i("TAG", "onResponse: $response")
            },
            Response.ErrorListener {
                Toast.makeText(this@AggprenActivity, "Request error", Toast.LENGTH_LONG).show()
                Log.i("TAG", "onErrorResponse: Didn't work")
            }) {

            override fun getHeaders(): Map<String, String> {
                val params = HashMap<String, String>()
                params["Authorization"] = serverKey
                params["Content-Type"] = contentType
                return params
            }
        }
        requestQueue.add(jsonObjectRequest)
    }

    override fun onBackPressed() {
        val intent= Intent(this, ChooseserviceActivity::class.java)
        intent.putExtra("evento", eventmain)
        startActivity(intent)
    }


}