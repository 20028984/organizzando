package it.uniupo.organizzando

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.*
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_maps.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException
import java.text.DecimalFormat
import java.util.*


internal class MapsaddservActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private var buttonsend: Button? = null
    private var addressbar: EditText? = null
    private var buttonconfirm: Button? = null
    private var customposition: LatLng = LatLng(0.0, 0.0)
    private var distance : Double = 0.0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        addressbar = findViewById<View>(R.id.address) as EditText
        buttonsend = findViewById<View>(R.id.sendmap) as Button
        buttonconfirm = findViewById<View>(R.id.confirmmap) as Button

        val service = intent.getSerializableExtra("servizio") as? Service

        buttonsend!!.setOnClickListener {
            customposition=getaddressposition()
            val cameraPosition = CameraPosition.Builder()
                .target(
                    LatLng(
                        customposition.latitude,
                        customposition.longitude
                    )
                ) // Sets the center of the map to location user
                .zoom(17f) // Sets the zoom
                .bearing(90f) // Sets the orientation of the camera to east
                .tilt(40f) // Sets the tilt of the camera to 30 degrees
                .build() // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            buttonconfirm!!.isEnabled = true
            buttonconfirm!!.isClickable = true
        }

        buttonconfirm!!.setOnClickListener{

            GlobalScope.launch(Dispatchers.Default) {
                val addresses: List<Address>
                val  geocoder = Geocoder(baseContext, Locale.getDefault())
                addresses = geocoder.getFromLocation(customposition.latitude, customposition.longitude, 1)
                val address = addresses[0].getAddressLine(0)
                try {
                    val geocoder = Geocoder(this@MapsaddservActivity, Locale.getDefault())
                    val addresses: List<Address> = geocoder.getFromLocation(customposition.latitude, customposition.longitude, 1)
                    val addresslist = addresses
                    val address: String = addresslist[0].getAddressLine(0)
                    Log.d("add", address)
                    val user2 = FirebaseAuth.getInstance().currentUser?.email
                    val prodotto = hashMapOf(
                        "descrizione" to service!!.description,
                        "immagine" to service.image,
                        "prezzo" to service.price,
                        "orario" to service.opening,
                        "nome" to service.name,
                        "categoria" to service.category,
                        "proprietario" to service.owner,
                        "luogo" to address,
                        "giorni" to service.days
                    )

                    service.place=address

                    db().collection("servizi").document(service.name)
                        .set(prodotto)
                        .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully written!") }
                        .addOnFailureListener { e -> Log.w(TAG, "Error writing document", e) }



                    addService(service)


                    val intent = Intent(this@MapsaddservActivity, FornitoremainActivity::class.java)
                    startActivity(intent)

                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }





        }


    }


    private val LOCATION_REQUEST_CODE = 101


    private fun requestPermission(permissionType: String, requestCode: Int) {
        ActivityCompat.requestPermissions(this, arrayOf(permissionType), requestCode
        )
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val permission = ContextCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_FINE_LOCATION)

        if (permission == PackageManager.PERMISSION_GRANTED) {
            mMap.isMyLocationEnabled = true
        } else {
            requestPermission(
                Manifest.permission.ACCESS_FINE_LOCATION,
                LOCATION_REQUEST_CODE)
        }

        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val criteria = Criteria()

        val location: Location? =
            locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false)!!)
        if (location != null) {
            mMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(
                        location.getLatitude(),
                        location.getLongitude()
                    ), 13f
                )
            )
            val cameraPosition = CameraPosition.Builder()
                .target(
                    LatLng(
                        location.getLatitude(),
                        location.getLongitude()
                    )
                ) // Sets the center of the map to location user
                .zoom(17f) // Sets the zoom
                .bearing(90f) // Sets the orientation of the camera to east
                .tilt(40f) // Sets the tilt of the camera to 30 degrees
                .build() // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        }



        mMap.setOnMapClickListener { p0 ->
            mMap.addMarker(p0.let {
                MarkerOptions()
                    .position(it)
                    .title("posizione click")
                    .snippet("Il tuo evento si svolgerà qui!")
            })
            customposition = LatLng(p0.latitude, p0.longitude)
            buttonconfirm!!.isEnabled = true
            buttonconfirm!!.isClickable = true
        }

        val mapSettings = mMap.uiSettings
        mapSettings.isZoomControlsEnabled = true
    }

    fun db(): FirebaseFirestore {
        return FirebaseFirestore.getInstance()
    }

    private fun getaddressposition(): LatLng {

        var latitude: Double
        var longitude: Double

        latitude=0.0
        longitude=0.0


        var geocodeMatches: List<Address>? = null

        try {
            geocodeMatches = Geocoder(this).getFromLocationName(
                addressbar!!.getText().toString(), 1)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        if (geocodeMatches != null) {
            latitude = geocodeMatches[0].latitude
            longitude = geocodeMatches[0].longitude
        }


        val position = LatLng(latitude, longitude)
        mMap.addMarker(MarkerOptions()
            .position(position)
            .title("il tuo indirizzo")
            .snippet("indirizzo del servizio"))

        return position
    }

    fun CalculationByDistance(StartP: LatLng, EndP: LatLng): Double {
        val Radius = 6371 // radius of earth in Km
        val lat1 = StartP.latitude
        val lat2 = EndP.latitude
        val lon1 = StartP.longitude
        val lon2 = EndP.longitude
        val dLat = Math.toRadians(lat2 - lat1)
        val dLon = Math.toRadians(lon2 - lon1)
        val a = (Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + (Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2)))
        val c = 2 * Math.asin(Math.sqrt(a))
        val valueResult = Radius * c
        val km = valueResult / 1
        val newFormat = DecimalFormat("####")
        val kmInDec: Int = Integer.valueOf(newFormat.format(km))
        val meter = valueResult % 1000
        val meterInDec: Int = Integer.valueOf(newFormat.format(meter))
        Log.i("Radius Value", "" + valueResult + "   KM  " + kmInDec
                + " Meter   " + meterInDec)
        return Radius * c
    }
}

