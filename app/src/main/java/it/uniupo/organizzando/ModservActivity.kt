package it.uniupo.organizzando

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.ContentValues.TAG
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import it.uniupo.organizzando.fragment.TimePickerFragment1
import it.uniupo.organizzando.fragment.TimePickerFragment2
import kotlinx.android.synthetic.main.activity_modserv.*
import java.io.ByteArrayOutputStream

class ModservActivity : AppCompatActivity() {
    private val PERMISSIONCODE = 1000;
    private val IMAGE_CAPTURE_CODE = 1001
    var image_uri: Uri? = null
    private var buttonupload: ImageView? = null
    private var buttonsubmit: Button? = null
    private var orario1text: TextView? = null
    private var orario2text: TextView? = null
    private var buttondelete: Button? = null
    private var imageURL: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_modserv)

        buttonupload = findViewById<View>(R.id.anteprima2) as ImageView
        buttonsubmit = findViewById<View>(R.id.btn_submit2) as Button
        buttondelete = findViewById<View>(R.id.btn_deleteserv) as Button
        orario1text = findViewById<View>(R.id.orarioinizio2) as TextView
        orario2text = findViewById<View>(R.id.orariofine2) as TextView
        val storage = FirebaseStorage.getInstance()

        val service = intent.getSerializableExtra("servizio") as? Service

        nome2.text=service!!.name
        descrizione2.setText(service.description)
        prezzo2.setText(service.price.toString())
        val orari = service.opening.split("-")
        orarioinizio2.text=orari[0]
        orariofine2.text=orari[1]


        val gsReference = storage.getReferenceFromUrl(service.image)



        gsReference.downloadUrl.addOnSuccessListener {Uri->

            imageURL = Uri.toString()

            Picasso.get().load(imageURL).into(anteprima2)

        }


        orario1text!!.setOnClickListener{
            TimePickerFragment1().show(supportFragmentManager, "timePicker")
        }
        orario2text!!.setOnClickListener{
            TimePickerFragment2().show(supportFragmentManager, "timePicker")
        }

        buttonupload!!.setOnClickListener {

            buttonsubmit!!.isEnabled = true
            buttonsubmit!!.isClickable = true

            if (checkSelfPermission(Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED ||
                checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED
            ) {
                //permission was not enabled
                val permission = arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                //show popup to request permission
                requestPermissions(permission, PERMISSIONCODE)
            } else {
                //permission already granted
                openCamera()
            }
        }


        buttondelete!!.setOnClickListener(){

            val builder = AlertDialog.Builder(this, R.style.AlertDialog)
            builder.setTitle("Conferma: ")
            builder.setMessage("Sei sicuro di voler cancellare il servizio?")
            builder.setPositiveButton(android.R.string.yes) { dialog, which ->


                db().collection("servizi").document(service.name)
                    .delete()
                    .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully deleted!") }
                    .addOnFailureListener { e -> Log.w(TAG, "Error deleting document", e) }


                val intent = Intent(this@ModservActivity, FornitoremainActivity::class.java)
                startActivity(intent)
            }

            builder.setNegativeButton("No") { dialog, which ->
                Toast.makeText(
                    applicationContext,
                    "operazione annullata!", Toast.LENGTH_SHORT
                ).show()
            }
            builder.show()
        }

        buttonsubmit!!.setOnClickListener{

            var downloadUri = ""

            val storageRef = storage.reference

            val desertRef = storageRef.child("images/${nome2.text}.jpg")

            // Delete the file
            desertRef.delete().addOnSuccessListener {
                // File deleted successfully
            }.addOnFailureListener {
                // Uh-oh, an error occurred!
            }

            val ImagesRef = storageRef.child("images/${nome2.text}.jpg")


            anteprima2.isDrawingCacheEnabled = true
            anteprima2.buildDrawingCache()
            val bitmap = (anteprima2.drawable as BitmapDrawable).bitmap
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val data = baos.toByteArray()

            val uploadTask = ImagesRef.putBytes(data)

            val urlTask = uploadTask.continueWithTask { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                }
                ImagesRef.downloadUrl
            }.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    downloadUri = task.result.toString()
                    val user2 = FirebaseAuth.getInstance().currentUser?.email
                    val servizio = hashMapOf(
                        "descrizione" to descrizione2.text.toString(),
                        "immagine" to downloadUri,
                        "prezzo" to prezzo2.text.toString().toLong(),
                        "orario" to orarioinizio2.text.toString()+" - "+orariofine2.text.toString(),
                        "nome" to service.name,
                        "categoria" to spinnercategorie2.selectedItem.toString(),
                        "proprietario" to user2.toString()
                    )

                    db().collection("servizi").document("${nome2.text}")
                        .set(servizio)
                        .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully written!") }
                        .addOnFailureListener { e -> Log.w(TAG, "Error writing document", e) }

                    val user = FirebaseAuth.getInstance().currentUser

                    val p = Service(nome2.text.toString(),descrizione2.text.toString(),downloadUri,prezzo2.text.toString().toLong(),spinnercategorie2.selectedItem.toString(),orarioinizio2.text.toString()+" - "+orariofine2.text.toString(),user.toString(),service.place,service.days)


                    addService(p)


                    val intent = Intent(this@ModservActivity, FornitoremainActivity::class.java)
                    startActivity(intent)


                } else {
                    Toast.makeText(applicationContext, "Errore!", Toast.LENGTH_SHORT).show()
                }
            }




            val intent = Intent(this@ModservActivity, FornitoremainActivity::class.java)
            startActivity(intent)
        }


    }


    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        image_uri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        //called when user presses ALLOW or DENY from Permission Request Popup
        when (requestCode) {
            PERMISSIONCODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    //permission from popup was granted
                    openCamera()
                } else {
                    //permission from popup was denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //called when image was captured from camera intent
        if (resultCode == Activity.RESULT_OK) {
            //set image captured to image view
            anteprima2.setImageURI(image_uri)
        }
    }

    fun db(): FirebaseFirestore {
        return FirebaseFirestore.getInstance()
    }

}