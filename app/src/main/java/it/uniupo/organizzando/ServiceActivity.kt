package it.uniupo.organizzando

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_service.*
import kotlinx.android.synthetic.main.service_recycler_view_item.previewIcon
import kotlinx.android.synthetic.main.price_review_title_section.*
import com.google.firebase.auth.*


class ServiceActivity : AppCompatActivity() {

    private var buttonprenota: Button? = null
    private var buttonmodify: Button? = null
    private var imageURL: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service)
        val db = FirebaseFirestore.getInstance()
        buttonprenota = findViewById<View>(R.id.btnPurchase) as Button
        buttonmodify = findViewById<View>(R.id.btnModify) as Button
        val service = intent.getSerializableExtra("Service") as? Service
        val event = intent.getSerializableExtra("Event") as? Event
        if (service!=null){
            Log.e("service", service.toString())
            loadViews(service)
        }

        val user = FirebaseAuth.getInstance().currentUser
        user?.let {
            val email = user.email.toString()
            val docRef = db.collection("utenti").document(email)
            docRef.get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        val ris = document.data?.get("tipo").toString()
                        Log.d(ContentValues.TAG, "DocumentSnapshot data: $ris")
                        if (ris == "1")
                        {
                            btnPurchase.visibility = View.GONE
                        }
                        else{
                            btnModify.visibility = View.GONE
                        }
                    } else {
                        Log.d(ContentValues.TAG, "No such document")
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d(ContentValues.TAG, "get failed with ", exception)
                }
        }


        buttonprenota!!.setOnClickListener {


            val intent = Intent(this@ServiceActivity, AggprenActivity::class.java)

            intent.putExtra("nome", titleService.text.toString())
            intent.putExtra("proprietario", service!!.owner)
            intent.putExtra("Event", event)
            intent.putExtra("servizio", service)
            startActivity(intent)

        }

        buttonmodify!!.setOnClickListener {

            val intent = Intent(this@ServiceActivity, ModservActivity::class.java)
            intent.putExtra("servizio", service)
            startActivity(intent)

        }

    }

    private fun loadViews(service: Service) {

        val storage = FirebaseStorage.getInstance()

        val gsReference = storage.getReferenceFromUrl(service.image)



        gsReference.downloadUrl.addOnSuccessListener {Uri->

            imageURL = Uri.toString()

            Picasso.get().load(imageURL).into(previewIcon)

        }

        titleService.text = service.name
        descService.text =   service.description
        servicePrice.text = "€${service.price}"
        categoriaserv.text = service.category
        orariserv.text = service.opening
        prop.text= "offerto da: ${service.owner}"
        var giorni = service.days.toString().replace("[","").replace("]","")
        giorni= "Giorni di apertura: "+giorni
        giorniapertura.text= giorni

    }
}