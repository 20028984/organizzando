package it.uniupo.organizzando.Fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.uniupo.organizzando.Adapter.PostAdapter
import it.uniupo.organizzando.Model.Post
import it.uniupo.organizzando.R
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class HomeFragment : Fragment() {

    private lateinit var eventname: String
    private var postAdapter:PostAdapter?=null
    private var postList:MutableList<Post>?=null
    private var followingList:MutableList<String>?=null


     override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view= inflater.inflate(R.layout.fragment_home, container, false)

        var recyclerView:RecyclerView?=null
         var recyclerViewStory:RecyclerView?=null

         recyclerView=view.findViewById(R.id.recycler_view_home)
         val linearlayoutManager=LinearLayoutManager(context)
         linearlayoutManager.reverseLayout=true
         //New posts at top
         linearlayoutManager.stackFromEnd=true
         recyclerView.layoutManager=linearlayoutManager
         //For Posts
         postList=ArrayList()
         postAdapter=context?.let { PostAdapter(it,postList as ArrayList<Post>) }
         recyclerView.adapter=postAdapter


         recyclerViewStory=view.findViewById(R.id.recycler_view_story)
         recyclerViewStory.setHasFixedSize(true)
         val linearlayoutManager2=LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
         recyclerViewStory.layoutManager=linearlayoutManager2


         retrievePosts()

        return view
    }

    private fun retrievePosts() {
        val pref = context?.getSharedPreferences("PREFS", Context.MODE_PRIVATE)
        if (pref != null) {
            this.eventname = pref.getString("event", "none")!!
        }
        val postRef=FirebaseDatabase.getInstance("https://organizzando-default-rtdb.europe-west1.firebasedatabase.app").reference.child("Posts").child(eventname)

         postRef.addValueEventListener(object : ValueEventListener
         {
             override fun onCancelled(error: DatabaseError) {

             }

             override fun onDataChange(p0: DataSnapshot)
             {
                 if(p0.exists()) {
                     postList?.clear()
                     for (snapshot in p0.children) {
                         val post = snapshot.getValue(Post::class.java)

                         postList!!.add(post!!)
                         postAdapter!!.notifyDataSetChanged()
                     }
                 }
             }

         })
    }

}
