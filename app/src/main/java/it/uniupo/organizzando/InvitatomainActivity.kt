package it.uniupo.organizzando

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_invitatomain.*
import it.uniupo.organizzando.R
import kotlinx.android.synthetic.main.activity_event.*
import kotlinx.android.synthetic.main.activity_organizzatoremain.*


class InvitatomainActivity : AppCompatActivity() {
    private var btnjoin: Button? = null
    private var btnback: ImageView? = null
    private var auth: FirebaseAuth? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invitatomain)


        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance()

        btnjoin = findViewById<View>(R.id.btnjoin) as Button
        btnback = findViewById<View>(R.id.iconaback11) as ImageView


        btnback!!.setOnClickListener{

            startIntent(GuesteventlistActivity::class.java)
        }

        btnjoin!!.setOnClickListener {
            val db = FirebaseFirestore.getInstance()
            var evento: Event
            db.collection("eventi")
                .get()
                .addOnSuccessListener { documents ->
                    for (document in documents) {
                        evento= Event(
                            document.data["nome"].toString(),
                            document.data["data"].toString(),
                            document.data["orario"].toString(),
                            document.data["budget"].toString().toLong(),
                            document.data["luogo"].toString(),
                            document.data["proprietario"].toString(),
                            document.data["categoria"].toString(),
                            document.data["budgetattuale"].toString().toLong(),
                            arrayListOf(document.data["servizi"].toString()),
                            document.data["codiceinvito"].toString(),
                            arrayListOf(document.data["invitati"].toString()),
                            arrayListOf(document.data["tipiservizio"].toString())
                        )
                        if(evento.invitecode==codejoin.text.toString())
                        {
                            val user = FirebaseAuth.getInstance().currentUser!!.email
                            db.collection("eventi").document(evento.name)
                                .update("invitati", FieldValue.arrayUnion(user))
                                .addOnSuccessListener { Log.d(ContentValues.TAG, "invitato aggiunto!")
                                startIntent(GuesteventlistActivity::class.java)
                                }
                                .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }
                        }else{
                            Toast.makeText(this, "Nessun evento corrisponde al codice inserito!", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
                .addOnFailureListener { exception ->
                    Log.w(ContentValues.TAG, "Error getting documents: ", exception)
                }


        }

    }

    private fun signOut() {
        auth!!.signOut()
    }

    override fun onResume() {
        super.onResume()
    }


    override fun onBackPressed() {
        startIntent(GuesteventlistActivity::class.java)
    }



    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }
}