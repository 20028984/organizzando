package it.uniupo.organizzando

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.*
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_aggpren.*
import kotlinx.android.synthetic.main.activity_meeting.*
import kotlinx.android.synthetic.main.price_review_title_section.*
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*


class MeetingActivity : AppCompatActivity() {

    private var buttonaccept: Button? = null
    private var buttondelete: Button? = null
    private var buttonsend: Button? = null
    private var buttoncalendar: Button? = null
    private var meetingmain: Meeting? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meeting)
        buttonaccept= findViewById<View>(R.id.btnaccept) as Button
        buttondelete = findViewById<View>(R.id.btndelete) as Button
        buttonsend = findViewById<View>(R.id.btnsendprev) as Button
        buttoncalendar = findViewById<View>(R.id.btnaddcalendar) as Button
        val meeting = intent.getSerializableExtra("Meeting") as? Meeting
        if (meeting!=null){
            Log.e("Meeting", meeting.toString())
            meetingmain= meeting
            loadViews(meeting)
        }

        val user = FirebaseAuth.getInstance().currentUser
        user?.let {
            val email = user.email.toString()
            val docRef = db().collection("utenti").document(email)
            docRef.get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        val ris = document.data?.get("tipo").toString()
                        Log.d(ContentValues.TAG, "DocumentSnapshot data: $ris")
                        if (ris == "2"){
                            if(meeting?.status!="in attesa di approvazione" && meeting?.status !="in programma")
                            {
                                btndelete.visibility = View.GONE
                            }
                            if(meeting!!.status=="passato")
                            {
                                btnaddcalendar.visibility=View.GONE
                            }
                            btnaccept.visibility = View.GONE
                            btnsendprev.visibility = View.GONE
                        }
                        else if (ris == "3")
                        {
                            btnaccept.visibility = View.GONE
                            btndelete.visibility = View.GONE
                            btnsendprev.visibility = View.GONE
                        }else
                        {
                            if(meeting?.status!="in attesa di approvazione" && meeting?.status !="in programma")
                            {
                                btndelete.visibility = View.GONE
                            }
                            if(meeting?.status!="in programma")
                            {
                                btnsendprev.visibility = View.GONE
                            }
                            if(meeting?.status!="in attesa di approvazione")
                            {
                                btnaccept.visibility = View.GONE
                            }
                            if(meeting!!.status=="passato")
                            {
                                btnaddcalendar.visibility=View.GONE
                            }

                        }
                    } else {
                        Log.d(ContentValues.TAG, "No such document")
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d(ContentValues.TAG, "get failed with ", exception)
                }
        }


        buttonaccept!!.setOnClickListener {


            db().collection("appuntamenti").document(meeting!!.codice)
                .update("stato", "in programma")
                .addOnSuccessListener { Log.d(ContentValues.TAG, "DocumentSnapshot successfully written!") }
                .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }
            Toast.makeText(this, "Appuntamento accettato!", Toast.LENGTH_SHORT).show()
            val docRef = db().collection("utenti").document(meeting.organizzatore)
            docRef.get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        val ris = document.data?.get("token").toString()
                        val user_token = ris
                        val notification_title = "Appuntamento confermato!"
                        val notification_des = "Il tuo appuntamento con ${meeting.fornitore} è stato approvato!"
                        FCMMessages().sendMessageSingle(
                            this@MeetingActivity,
                            user_token,
                            notification_title,
                            notification_des,
                            null
                        )

                    } else {
                        Log.d(ContentValues.TAG, "No such document")
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d(ContentValues.TAG, "get failed with ", exception)
                }
            startIntent(MeetinglistActivity::class.java)
        }

        buttondelete!!.setOnClickListener {
            db().collection("appuntamenti").document(meeting!!.codice)
                .update("stato", "annullato")
                .addOnSuccessListener { Log.d(ContentValues.TAG, "DocumentSnapshot successfully written!") }
                .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }
            Toast.makeText(this, "Appuntamento annullato!", Toast.LENGTH_SHORT).show()
            startIntent(MeetinglistActivity::class.java)
        }

        buttonsend!!.setOnClickListener {

            showdialog(meeting!!)

        }

    }

    fun addCalendarEvent(view: View) {
        val calendarEvent: Calendar = Calendar.getInstance()
        val intent = Intent(Intent.ACTION_EDIT)
        var orario = meetingmain!!.ora+":00"
        val stringa = meetingmain!!.data+" "+orario
        val date1: Date = SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(stringa) as Date
        intent.type = "vnd.android.cursor.item/event"
        intent.putExtra("beginTime", date1.time)
        intent.putExtra("allDay", false)
        intent.putExtra("rule", "FREQ=YEARLY")
        intent.putExtra("title", "Appuntamento per "+ meetingmain!!.event)
        startActivity(intent)
    }

    private fun loadViews(meeting: Meeting) {

        dataapp.text =   "Data " +meeting.data
        oraapp.text = "Ora: "+ meeting.ora
        organizzatoreapp.text = "Organizzatore: "+ meeting.organizzatore
        fornitoreapp.text = "Fornitore: "+meeting.fornitore
        statusapp.text = "Stato: "+meeting.status
        extraappact.text = "Extra: "+meeting.extra
    }

    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }

    fun db(): FirebaseFirestore {
        return FirebaseFirestore.getInstance()
    }

    fun showdialog(meeting: Meeting){
        val builder: AlertDialog.Builder = android.app.AlertDialog.Builder(this, R.style.AlertDialog)
        builder.setTitle("Inserisci importo preventivo")

        // Set up the input
        val input = EditText(this)
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setHint("Inserisci importo")
        input.inputType = InputType.TYPE_NUMBER_FLAG_DECIMAL
        builder.setView(input)

        // Set up the buttons
        builder.setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
            // Here you get get input text from the Edittext
            val testo = input.text.toString()
            if (testo=="")
            {
                Toast.makeText(this, "Inserisci un importo valido!", Toast.LENGTH_SHORT).show()
            }else
            {

                val stamp = Timestamp(System.currentTimeMillis()).toString()
                val item = Quote(
                    stamp,
                    meeting.nomeserv,
                    meeting.event,
                    meeting.organizzatore,
                    meeting.fornitore,
                    "da accettare",
                    "nessuno",
                    "nessuna",
                    "nessuna"
                )

                addQuote(item)


                val preventivo = hashMapOf(
                    "codice" to stamp,
                    "servizio" to meeting.nomeserv,
                    "evento" to meeting.event,
                    "organizzatore" to meeting.organizzatore,
                    "fornitore" to meeting.fornitore,
                    "stato" to "da accettare",
                    "metodo" to "nessuno",
                    "modalita" to "nessuna",
                    "conferma" to "nessuna"
                )

                db().collection("preventivi").document(stamp)
                    .set(preventivo)
                    .addOnSuccessListener { Log.d(ContentValues.TAG, "DocumentSnapshot successfully written!") }
                    .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }


                db().collection("appuntamenti").document(meeting.codice)
                    .update("stato", "passato")
                    .addOnSuccessListener { Log.d(ContentValues.TAG, "DocumentSnapshot successfully written!") }
                    .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }
                Toast.makeText(this, "Preventivo inviato!", Toast.LENGTH_SHORT).show()
                val docRef = db().collection("utenti").document(meeting.organizzatore)
                docRef.get()
                    .addOnSuccessListener { document ->
                        if (document != null) {
                            val ris = document.data?.get("token").toString()
                            val user_token = ris
                            val notification_title = "Nuovo appuntamento"
                            val notification_des = "${meeting.fornitore} ha caricato un nuovo preventivo per il servizio ${meeting.nomeserv}"
                            FCMMessages().sendMessageSingle(
                                this@MeetingActivity,
                                user_token,
                                notification_title,
                                notification_des,
                                null
                            )

                        } else {
                            Log.d(ContentValues.TAG, "No such document")
                        }
                    }
                    .addOnFailureListener { exception ->
                        Log.d(ContentValues.TAG, "get failed with ", exception)
                    }
                startIntent(FornitoretruemainActivity::class.java)
            }

        })
        builder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, which -> dialog.cancel() })

        builder.show()
    }
}