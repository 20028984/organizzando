package it.uniupo.organizzando


import android.app.Activity
import android.content.ContentValues
import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import it.uniupo.organizzando.fragment.DatePickerFragment2
import it.uniupo.organizzando.fragment.TimePickerFragment4
import kotlinx.android.synthetic.main.activity_addevent.*
import kotlinx.android.synthetic.main.activity_event.*
import kotlinx.android.synthetic.main.activity_notificationoption.*

class NotificationoptionActivity : AppCompatActivity() {
    private var buttonsubmit: Button? = null
    private var backbutton: ImageView? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notificationoption)

        buttonsubmit = findViewById<View>(R.id.btn_submit2) as Button
        backbutton = findViewById<View>(R.id.iconaback12) as ImageView

        val db = FirebaseFirestore.getInstance()
        val user = FirebaseAuth.getInstance().currentUser!!.email

        postcomm.visibility= View.GONE
        mieipost.visibility= View.GONE
        nuovipost.visibility= View.GONE

        backbutton!!.setOnClickListener{
            startActivity(Intent(this, GuesteventlistActivity::class.java))
        }



        buttonsubmit!!.setOnClickListener {

            if (radio_pirates.isChecked)
            {
                db.collection("utenti").document(user!!)
                    .update("notifiche","tutte")
                db.collection("utenti").document(user!!)
                    .update("nuovipost","si")
                db.collection("utenti").document(user)
                    .update("postcommentati","si")
                db.collection("utenti").document(user)
                    .update("mieicommenti","si")
            }else
            {
                db.collection("utenti").document(user!!)
                    .update("nuovipost","no")
                db.collection("utenti").document(user)
                    .update("postcommentati","no")
                db.collection("utenti").document(user)
                    .update("mieicommenti","no")
                if (nuovipost.isChecked){
                    db.collection("utenti").document(user)
                        .update("nuovipost","si")
                }
                if(postcomm.isChecked)
                {
                    db.collection("utenti").document(user)
                        .update("postcommentati","si")
                }
                if(mieipost.isChecked)
                {
                    db.collection("utenti").document(user)
                        .update("mieicommenti","si")
                }
                if(!mieipost.isChecked && !postcomm.isChecked && !nuovipost.isChecked)
                {
                    db.collection("utenti").document(user)
                        .update("notifiche","nessuna")
                }
            }

            Toast.makeText(this, "Salvato!", Toast.LENGTH_SHORT).show()

        }


    }


    fun onRadioButtonClicked(view: View) {
        if (view is RadioButton) {
            val checked = view.isChecked
            when (view.getId()) {
                R.id.radio_pirates ->
                    if (checked) {
                        postcomm.visibility= View.GONE
                        mieipost.visibility= View.GONE
                        nuovipost.visibility= View.GONE
                    }
                R.id.radio_ninjas ->
                    if (checked) {
                        postcomm.visibility= View.VISIBLE
                        mieipost.visibility= View.VISIBLE
                        nuovipost.visibility= View.VISIBLE
                    }
            }
        }
    }

    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }
}