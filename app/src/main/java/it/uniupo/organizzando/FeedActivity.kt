package it.uniupo.organizzando

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import it.uniupo.organizzando.Fragments.HomeFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class FeedActivity : AppCompatActivity() {


    internal var selectedFragment:Fragment?=null

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.nav_addpost -> {
                item.isChecked=false
                val event = intent.getSerializableExtra("Event") as? Event
                val intent = Intent(this@FeedActivity, AddPostActivity::class.java)
                intent.putExtra("Event", event)
                startActivity(intent)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feed)
        setSupportActionBar(findViewById(R.id.home_toolbar))

        val event = intent.getSerializableExtra("Event") as? Event

        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        val prefs: SharedPreferences.Editor? =
            getSharedPreferences("PREFS", Context.MODE_PRIVATE)
                .edit().apply { putString("event", event!!.name); apply() }
        moveToFragment(HomeFragment())
    }

    private fun moveToFragment(fragment:Fragment)
    {
        val fragmentTrans=supportFragmentManager.beginTransaction()
        fragmentTrans.replace(R.id.fragment_container,fragment)
        fragmentTrans.commit()
    }
}