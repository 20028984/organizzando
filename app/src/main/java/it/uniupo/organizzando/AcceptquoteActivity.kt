package it.uniupo.organizzando

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_acceptquote.*


class AcceptquoteActivity : AppCompatActivity() {

    private var buttonconfirm: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_acceptquote)
        val db = FirebaseFirestore.getInstance()
        buttonconfirm = findViewById<View>(R.id.buttonacceptfinal) as Button

        val event = intent.getSerializableExtra("Event") as? Event
        val service = intent.getSerializableExtra("Service") as? Service
        val quote = intent.getSerializableExtra("Quote") as? Quote

        var nuovobudget: Long = 0




            nuovobudget = event!!.actualbudget.toString().toLong()-service!!.price.toString().toLong()



        val stringa = "I, tuo budget sarà ridotto a $nuovobudget"

        budgetinfo.text= stringa


        buttonconfirm!!.setOnClickListener {

        val modalitaId = radio_group_modalita.checkedRadioButtonId
        val modalitastring = resources.getResourceEntryName(modalitaId)

        val metodoId = radio_group_metodo.checkedRadioButtonId
        val metodostring = resources.getResourceEntryName(metodoId)

            db().collection("eventi").document(event.name)
                .update("budgetattuale", nuovobudget)
                .addOnSuccessListener { Log.d(ContentValues.TAG, "budget aggiornato!") }
                .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }

            db.collection("eventi").document(event.name)
                .update("servizi", FieldValue.arrayUnion(service.name))
                .addOnSuccessListener { Log.d(ContentValues.TAG, "servizio aggiunto!")
                }
                .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }

            db().collection("preventivi").document(quote!!.code)
                .update("metodo", modalitastring)
                .addOnSuccessListener { Log.d(ContentValues.TAG, "Metodo aggiornato!") }
                .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }


            db().collection("preventivi").document(quote.code)
                .update("modalita", metodostring)
                .addOnSuccessListener { Log.d(ContentValues.TAG, "Modalità aggiornata!") }
                .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }

            if(sulposto.isChecked)
            {
                db().collection("preventivi").document(quote.code)
                    .update("stato", "da pagare")
                    .addOnSuccessListener { Log.d(ContentValues.TAG, "Stato aggiornato!") }
                    .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }
            }else{
                db().collection("preventivi").document(quote.code)
                    .update("stato", "accettato")
                    .addOnSuccessListener { Log.d(ContentValues.TAG, "Stato aggiornato!") }
                    .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }
            }


            val docRef = db().collection("utenti").document(quote.supplier)
            docRef.get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        val ris = document.data?.get("token").toString()
                        val user_token = ris
                        val notification_title = "Preventivo Accettato!"
                        val notification_des = "${quote.manager} ha accettato il tuo preventivo per il servizio ${quote.serv}"
                        FCMMessages().sendMessageSingle(
                            this@AcceptquoteActivity,
                            user_token,
                            notification_title,
                            notification_des,
                            null
                        )

                    } else {
                        Log.d(ContentValues.TAG, "No such document")
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d(ContentValues.TAG, "get failed with ", exception)
                }
            Toast.makeText(this, "preventivo accettato!", Toast.LENGTH_SHORT).show()
            startIntent(QuotelistActivity::class.java)

        }


    }

    fun db(): FirebaseFirestore {
        return FirebaseFirestore.getInstance()
    }


    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }

}