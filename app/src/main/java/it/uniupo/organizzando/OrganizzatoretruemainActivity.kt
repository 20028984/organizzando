package it.uniupo.organizzando

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_organizzatoretrue.*

class OrganizzatoretruemainActivity : AppCompatActivity() {
    private var btnevent: Button? = null
    private var btnoption: Button? = null
    private var btnquote: Button? = null
    private var btnloguot: Button? = null
    private var auth: FirebaseAuth? = null
    private var btnmeeting: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_organizzatoretrue)


        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance()
        btnevent = findViewById<View>(R.id.btn_myevent) as Button
        btnoption = findViewById<View>(R.id.btn_myoption) as Button
        btnquote = findViewById<View>(R.id.btn_myquote) as Button
        btnloguot = findViewById<View>(R.id.btn_mylogout) as Button
        btnmeeting = findViewById<View>(R.id.btn_mymeeting) as Button


        val user = FirebaseAuth.getInstance().currentUser?.email

        val stringa = "benvenuto "+ user+"!"

        titletrueorg.text= stringa

        btnevent!!.setOnClickListener(View.OnClickListener {
            startIntent(OrganizzatoremainActivity::class.java)
        })

        btnoption!!.setOnClickListener(View.OnClickListener {
            startIntent(OptionsAccountActivity::class.java)
        })

        btnquote!!.setOnClickListener(View.OnClickListener {
            startIntent(QuotelistActivity::class.java)
        })


        btnmeeting!!.setOnClickListener {
            startIntent(MeetinglistActivity::class.java)
        }

        btnloguot!!.setOnClickListener(View.OnClickListener {
            signOut()
            startIntent(LoginActivity::class.java)
        })
    }

    fun signOut() {
        auth!!.signOut()
    }

    override fun onResume() {
        super.onResume()
    }

    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }
}