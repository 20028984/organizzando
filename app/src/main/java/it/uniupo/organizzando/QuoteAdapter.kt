package it.uniupo.organizzando

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.quote_recycler_view_item.view.*


class QuoteAdapter (var quotes: ArrayList<Quote>) : RecyclerView.Adapter<QuoteAdapter.QuoteViewHolder>(){


    fun updateQuote(newQuote: ArrayList<Quote>) {
        quotes.clear()
        quotes.addAll(newQuote)
        notifyDataSetChanged()
    }


    private fun launchNextScreen(context: Context, Quote: Quote): Intent {
        val intent = Intent(context, QuoteActivity::class.java)
        intent.putExtra("Quote", Quote)
        return intent
    }


    override fun onCreateViewHolder( parent: ViewGroup, viewType: Int) : QuoteViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.quote_recycler_view_item, parent, false)

        return QuoteViewHolder(cellForRow).listen { pos, _ ->
            val Quote:Quote = quotes[pos]
            parent.context.startActivity(launchNextScreen(parent.context, Quote))
        }

    }


    override fun getItemCount() = quotes.size


    override fun onBindViewHolder(holder: QuoteViewHolder, position: Int) {
        holder.bind(quotes[position])
    }



    class QuoteViewHolder(view: View) : RecyclerView.ViewHolder(view) {


        private val nomeev = view.nomeeventoprev
        private val nomeserv= view.nomeservprev
        private val stato = view.statoprev



        fun bind(quote: Quote) {

            nomeev.text = quote.event
            nomeserv.text = quote.serv
            stato.text= quote.status
        }
    }


}

