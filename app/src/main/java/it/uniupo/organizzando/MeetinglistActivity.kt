package it.uniupo.organizzando

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_event.*
import kotlinx.android.synthetic.main.activity_meetinglist.*
import kotlinx.android.synthetic.main.activity_service.*

class MeetinglistActivity : AppCompatActivity() {
    private var auth: FirebaseAuth? = null
    private var tipoutente: String? = null
    private var btnback: ImageView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meetinglist)

        val db = FirebaseFirestore.getInstance()


        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance()
        btnback = findViewById<View>(R.id.iconaback9) as ImageView

        val user = FirebaseAuth.getInstance().currentUser
        user?.let {
            val email = user.email.toString()
            val docRef = db.collection("utenti").document(email)
            docRef.get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        tipoutente = document.data?.get("tipo").toString()
                        Log.d(ContentValues.TAG, "DocumentSnapshot data: $tipoutente")
                    } else {
                        Log.d(ContentValues.TAG, "No such document")
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d(ContentValues.TAG, "get failed with ", exception)
                }
        }


        val allMeetingList: ArrayList<Meeting> = getAllMeeting()

        allMeetingList.clear()

        val user2 = FirebaseAuth.getInstance().currentUser?.email

        var usermeetingList: ArrayList<Meeting> = getAllMeeting()

        usermeetingList.clear()

        db.collection("appuntamenti")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    allMeetingList.add(
                        Meeting(
                        document.data["codice"].toString(),
                        document.data["nomeserv"].toString(),
                        document.data["organizzatore"].toString(),
                        document.data["fornitore"].toString(),
                        document.data["data"].toString(),
                        document.data["ora"].toString(),
                        document.data["stato"].toString(),
                        document.data["extra"].toString(),
                        document.data["evento"].toString(),
                    )
                    )
                }
                usermeetingList=allMeetingList
                for(e in usermeetingList)
                {
                    if(tipoutente=="1")
                    {
                        if(e.fornitore!=user2) {
                            usermeetingList.remove(e)
                        }
                    }else
                    {
                        if(e.organizzatore!=user2) {
                            usermeetingList.remove(e)
                        }
                    }
                }
                Log.d(ContentValues.TAG, "Numero appuntamenti: ${usermeetingList.size}")
                recyclerViewapp.layoutManager = GridLayoutManager(this, 1)
                recyclerViewapp.adapter = MeetingAdapter(meetings = usermeetingList)
            }
            .addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents: ", exception)
            }

        btnback!!.setOnClickListener{
            val email = user!!.email.toString()
            val docRef = db.collection("utenti").document(email)
            docRef.get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        val ris = document.data?.get("tipo").toString()
                        Log.d(ContentValues.TAG, "DocumentSnapshot data: $ris")
                        if (ris == "2")
                        {
                            startIntent(OrganizzatoretruemainActivity::class.java)
                        }else
                        {
                            startIntent(FornitoretruemainActivity::class.java)
                        }
                    } else {
                        Log.d(ContentValues.TAG, "No such document")
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d(ContentValues.TAG, "get failed with ", exception)
                }

        }

    }

    override fun onResume() {
        super.onResume()
    }

    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }

    override fun onBackPressed() {
        val db = FirebaseFirestore.getInstance()
        val email = FirebaseAuth.getInstance().currentUser!!.email.toString()
        val docRef = db.collection("utenti").document(email)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    val ris = document.data?.get("tipo").toString()
                    Log.d(ContentValues.TAG, "DocumentSnapshot data: $ris")
                    if (ris == "2")
                    {
                        startIntent(OrganizzatoretruemainActivity::class.java)
                    }else
                    {
                        startIntent(FornitoretruemainActivity::class.java)
                    }
                } else {
                    Log.d(ContentValues.TAG, "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d(ContentValues.TAG, "get failed with ", exception)
            }
    }

}