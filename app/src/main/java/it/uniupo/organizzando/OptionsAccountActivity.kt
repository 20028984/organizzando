package it.uniupo.organizzando

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuth.AuthStateListener
import androidx.appcompat.widget.Toolbar
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_event.*


class OptionsAccountActivity : AppCompatActivity() {
    private var btnChangeEmail: Button? = null
    private var btnChangePassword: Button? = null
    private var btnSendResetEmail: Button? = null
    private var btnRemoveUser: Button? = null
    private var changeEmail: Button? = null
    private var changePassword: Button? = null
    private var sendEmail: Button? = null
    private var notoption: Button? = null
    private var remove: Button? = null
    private var signOut: Button? = null
    private var oldEmail: EditText? = null
    private var newEmail: EditText? = null
    private var password: EditText? = null
    private var newPassword: EditText? = null
    private var progressBar: ProgressBar? = null
    private var authListener: AuthStateListener? = null
    private var auth: FirebaseAuth? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_option)
        val toolbar: Toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        toolbar.setTitle(getString(R.string.app_name))
        setSupportActionBar(toolbar)

        //get firebase auth instance
        auth = FirebaseAuth.getInstance()

        //get current user
        val user = FirebaseAuth.getInstance().currentUser
        authListener = AuthStateListener { firebaseAuth ->
            val user2 = firebaseAuth.currentUser
            if (user2 == null) {
                // user auth state is changed - user is null
                // launch login activity
                startActivity(Intent(this@OptionsAccountActivity, LoginActivity::class.java))
                finish()
            }
        }
        btnChangeEmail = findViewById<View>(R.id.change_email_button) as Button
        btnChangePassword = findViewById<View>(R.id.change_password_button) as Button
        btnSendResetEmail = findViewById<View>(R.id.sending_pass_reset_button) as Button
        btnRemoveUser = findViewById<View>(R.id.remove_user_button) as Button
        changeEmail = findViewById<View>(R.id.changeEmail) as Button
        changePassword = findViewById<View>(R.id.changePass) as Button
        notoption = findViewById<View>(R.id.notification_option) as Button
        sendEmail = findViewById<View>(R.id.send) as Button
        remove = findViewById<View>(R.id.remove) as Button
        signOut = findViewById<View>(R.id.sign_out) as Button
        oldEmail = findViewById<View>(R.id.old_email) as EditText
        newEmail = findViewById<View>(R.id.new_email) as EditText
        password = findViewById<View>(R.id.password) as EditText
        newPassword = findViewById<View>(R.id.newPassword) as EditText
        oldEmail!!.visibility = View.GONE
        newEmail!!.visibility = View.GONE
        password!!.visibility = View.GONE
        newPassword!!.visibility = View.GONE
        changeEmail!!.visibility = View.GONE
        changePassword!!.visibility = View.GONE
        sendEmail!!.visibility = View.GONE
        remove!!.visibility = View.GONE
        progressBar = findViewById<View>(R.id.progressBar) as ProgressBar
        if (progressBar != null) {
            progressBar!!.visibility = View.GONE
        }
        val email = user!!.email.toString()
        val db = FirebaseFirestore.getInstance()
        db.collection("utenti").document(email)
            .get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    val ris = document.data?.get("tipo").toString()
                    Log.d(ContentValues.TAG, "DocumentSnapshot data: $ris")
                    if (ris != "3")
                    {
                        notoption!!.visibility = View.GONE
                    }
                } else {
                    Log.d(ContentValues.TAG, "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d(ContentValues.TAG, "get failed with ", exception)
            }
        btnChangeEmail!!.setOnClickListener {
            oldEmail!!.visibility = View.GONE
            newEmail!!.visibility = View.VISIBLE
            password!!.visibility = View.GONE
            newPassword!!.visibility = View.GONE
            changeEmail!!.visibility = View.VISIBLE
            changePassword!!.visibility = View.GONE
            sendEmail!!.visibility = View.GONE
            remove!!.visibility = View.GONE
        }
        changeEmail!!.setOnClickListener {
            progressBar!!.visibility = View.VISIBLE
            if (user != null && newEmail!!.text.toString().trim { it <= ' ' } != "") {
                user.updateEmail(newEmail!!.text.toString().trim { it <= ' ' })
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(
                                this@OptionsAccountActivity,
                                "Email address is updated. Please sign in with new email id!",
                                Toast.LENGTH_LONG
                            ).show()
                            signOut()
                            progressBar!!.visibility = View.GONE
                        } else {
                            Toast.makeText(
                                this@OptionsAccountActivity,
                                "Failed to update email!",
                                Toast.LENGTH_LONG
                            ).show()
                            progressBar!!.visibility = View.GONE
                        }
                    }
            } else if (newEmail!!.text.toString().trim { it <= ' ' } == "") {
                newEmail!!.error = "Enter email"
                progressBar!!.visibility = View.GONE
            }
        }
        btnChangePassword!!.setOnClickListener {
            oldEmail!!.visibility = View.GONE
            newEmail!!.visibility = View.GONE
            password!!.visibility = View.GONE
            newPassword!!.visibility = View.VISIBLE
            changeEmail!!.visibility = View.GONE
            changePassword!!.visibility = View.VISIBLE
            sendEmail!!.visibility = View.GONE
            remove!!.visibility = View.GONE
        }
        changePassword!!.setOnClickListener {
            progressBar!!.visibility = View.VISIBLE
            if (user != null && newPassword!!.text.toString().trim { it <= ' ' } != "") {
                if (newPassword!!.text.toString().trim { it <= ' ' }.length < 6) {
                    newPassword!!.error = "Password too short, enter minimum 6 characters"
                    progressBar!!.visibility = View.GONE
                } else {
                    user.updatePassword(newPassword!!.text.toString().trim { it <= ' ' })
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                Toast.makeText(
                                    this@OptionsAccountActivity,
                                    "Password is updated, sign in with new password!",
                                    Toast.LENGTH_SHORT
                                ).show()
                                signOut()
                                progressBar!!.visibility = View.GONE
                            } else {
                                Toast.makeText(
                                    this@OptionsAccountActivity,
                                    "Failed to update password!",
                                    Toast.LENGTH_SHORT
                                ).show()
                                progressBar!!.visibility = View.GONE
                            }
                        }
                }
            } else if (newPassword!!.text.toString().trim { it <= ' ' } == "") {
                newPassword!!.error = "Enter password"
                progressBar!!.visibility = View.GONE
            }
        }
        notoption!!.setOnClickListener {
            val intent = Intent(this, NotificationoptionActivity::class.java)
            startActivity(intent)
        }
        btnSendResetEmail!!.setOnClickListener {
            oldEmail!!.visibility = View.VISIBLE
            newEmail!!.visibility = View.GONE
            password!!.visibility = View.GONE
            newPassword!!.visibility = View.GONE
            changeEmail!!.visibility = View.GONE
            changePassword!!.visibility = View.GONE
            sendEmail!!.visibility = View.VISIBLE
            remove!!.visibility = View.GONE
        }
        sendEmail!!.setOnClickListener {
            progressBar!!.visibility = View.VISIBLE
            if (oldEmail!!.text.toString().trim { it <= ' ' } != "") {
                auth!!.sendPasswordResetEmail(oldEmail!!.text.toString().trim { it <= ' ' })
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(
                                this@OptionsAccountActivity,
                                "Reset password email is sent!",
                                Toast.LENGTH_SHORT
                            ).show()
                            progressBar!!.visibility = View.GONE
                        } else {
                            Toast.makeText(
                                this@OptionsAccountActivity,
                                "Failed to send reset email!",
                                Toast.LENGTH_SHORT
                            ).show()
                            progressBar!!.visibility = View.GONE
                        }
                    }
            } else {
                oldEmail!!.error = "Enter email"
                progressBar!!.visibility = View.GONE
            }
        }
        btnRemoveUser!!.setOnClickListener {
            progressBar!!.visibility = View.VISIBLE
            user?.delete()?.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Toast.makeText(
                        this@OptionsAccountActivity,
                        "Your profile is deleted:( Create a account now!",
                        Toast.LENGTH_SHORT
                    ).show()
                    startActivity(Intent(this@OptionsAccountActivity, SignupActivity::class.java))
                    finish()
                    progressBar!!.visibility = View.GONE
                } else {
                    Toast.makeText(
                        this@OptionsAccountActivity,
                        "Failed to delete your account!",
                        Toast.LENGTH_SHORT
                    ).show()
                    progressBar!!.visibility = View.GONE
                }
            }
        }
        signOut!!.setOnClickListener { signOut() }
    }

    //sign out method
    fun signOut() {
        auth!!.signOut()
    }

    override fun onResume() {
        super.onResume()
        progressBar!!.visibility = View.GONE
    }

    public override fun onStart() {
        super.onStart()
        auth!!.addAuthStateListener(authListener!!)
    }

    public override fun onStop() {
        super.onStop()
        if (authListener != null) {
            auth!!.removeAuthStateListener(authListener!!)
        }
    }
}