package it.uniupo.organizzando

import android.R.*
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class FornitoreregActivity : AppCompatActivity() {
    private var inputnome: EditText? = null
    private var inputcognome: EditText? = null
    private var inputdata: EditText? = null
    private var inputluogonasc: EditText? = null
    private var inputnomeaz: EditText? = null
    private var inputluogoaz: EditText? = null
    private var inputiva: EditText? = null
    private var inputiban: EditText? = null
    private var btnregister: Button? = null
    private var checkpaypal: CheckBox? = null
    private var checkcarta: CheckBox? = null
    private var checkbonifico: CheckBox? = null
    private var progressBar: ProgressBar? = null
    private var auth: FirebaseAuth? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fornitorereg)
        val db = FirebaseFirestore.getInstance()
        val TAG = SignupActivity::class.java.simpleName



        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance()
        inputnome = findViewById<View>(R.id.nome) as EditText
        inputcognome = findViewById<View>(R.id.cognome) as EditText
        inputdata = findViewById<View>(R.id.datanascita) as EditText
        inputluogonasc = findViewById<View>(R.id.luogonascita) as EditText
        inputnomeaz = findViewById<View>(R.id.nomeazienda) as EditText
        inputluogoaz = findViewById<View>(R.id.indazienda) as EditText
        inputiva = findViewById<View>(R.id.piva) as EditText
        inputiban = findViewById<View>(R.id.iban) as EditText
        btnregister = findViewById<View>(R.id.register) as Button
        checkpaypal = findViewById<View>(R.id.paypal) as CheckBox
        checkcarta = findViewById<View>(R.id.carta) as CheckBox
        checkbonifico = findViewById<View>(R.id.bonifico) as CheckBox
        progressBar = findViewById<View>(R.id.progressBar) as ProgressBar

        val intent = intent
        val email = intent.getStringExtra("email")!!
        val password = intent.getStringExtra("password")!!

        btnregister!!.setOnClickListener(View.OnClickListener {
            val nome = inputnome!!.text.toString().trim { it <= ' ' }
            val cognome = inputcognome!!.text.toString().trim { it <= ' ' }
            val datanasc = inputdata!!.text.toString().trim { it <= ' ' }
            val luogonasc = inputluogonasc!!.text.toString().trim { it <= ' ' }
            val nomeaz = inputnomeaz!!.text.toString().trim { it <= ' ' }
            val luogoaz = inputluogoaz!!.text.toString().trim { it <= ' ' }
            val piva= inputiva!!.text.toString().trim { it <= ' ' }
            val iban = inputiban!!.text.toString().trim { it <= ' ' }
            val paypal = checkpaypal!!.isChecked
            val carta = checkcarta!!.isChecked
            val bonifico = checkbonifico!!.isChecked
            if (TextUtils.isEmpty(nome)) {
                Toast.makeText(applicationContext, "Inserisci il nome!", Toast.LENGTH_SHORT)
                    .show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(cognome)) {
                Toast.makeText(applicationContext, "Inserisci il cognome!", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(datanasc)) {
                Toast.makeText(applicationContext, "Inserisci la data di nascita!", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(luogonasc)) {
                Toast.makeText(applicationContext, "Inserisci il tuo luogo di nascita", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(nomeaz)) {
                Toast.makeText(applicationContext, "Inserisci il nome della tua azienda", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(luogoaz)) {
                Toast.makeText(applicationContext, "Inserisci la sede della tua azienda", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(piva)) {
                Toast.makeText(applicationContext, "Inserisci la partita IVA della tua azienda", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(iban)) {
                Toast.makeText(applicationContext, "Inserisci l'IBAN della tua azienda", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if(!paypal && !carta && !bonifico)
            {
                Toast.makeText(applicationContext, "Devi secgliere almeno un metodo!", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            progressBar!!.visibility = View.VISIBLE
            //create user
            auth!!.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(
                    this@FornitoreregActivity
                ) { task ->
                    Toast.makeText(
                        this@FornitoreregActivity,
                        "Utente creato con successo!",
                        Toast.LENGTH_SHORT
                    ).show()
                    progressBar!!.visibility = View.GONE
                    // If sign in fails, display a message to the user. If sign in succeeds
                    // the auth state listener will be notified and logic to handle the
                    // signed in user can be handled in the listener.
                    if (!task.isSuccessful) {
                        Toast.makeText(
                            this@FornitoreregActivity, "Authentication failed." + task.exception,
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {

                        val dati = hashMapOf(
                            "tipo" to 1,
                            "nome" to nome,
                            "cognome" to cognome,
                            "datanasc" to datanasc,
                            "luogonasc" to luogonasc,
                            "nomeaz" to nomeaz,
                            "luogoaz" to luogoaz,
                            "piva" to piva,
                            "iban" to iban,
                            "paypal" to paypal.toString(),
                            "carta" to carta.toString(),
                            "bonifico" to bonifico.toString()
                        )

                        db.collection("utenti").document(email)
                            .set(dati)
                            .addOnSuccessListener { documentReference ->
                                Log.d(TAG, "Document successful written!")
                            }
                            .addOnFailureListener { e ->
                                Log.w(TAG, "Error adding document", e)
                            }

                        startIntent(LoginActivity::class.java)

                        finish()
                    }
                }
        })
    }

    override fun onResume() {
        super.onResume()
        progressBar!!.visibility = View.GONE
    }

    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }
}