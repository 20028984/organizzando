package it.uniupo.organizzando

import java.io.Serializable


var listService = ArrayList<Service>()
var listMeeting = ArrayList<Meeting>()
var listEvent = ArrayList<Event>()
var listQuote = ArrayList<Quote>()



data class Service(var name: String, var description: String, var image: String, var price: Long, var category: String, var opening: String, var owner: String, var place: String, var days:ArrayList<String>): Serializable
data class Meeting(var codice: String,var nomeserv:String, var organizzatore: String, var fornitore: String, var data: String, var ora: String, var status: String, var extra: String, var event: String): Serializable
data class Event(var name: String,var date:String, var hour: String, var budget: Long, var place: String, var owner: String, var category: String, var actualbudget: Long, var services: ArrayList<String>, var invitecode: String, var guests: ArrayList<String>, var typeserv: ArrayList<String>): Serializable
data class Quote(var code: String,var serv:String,var event: String, var manager: String, var supplier: String, var status: String, var method: String, var modality: String, var confirm: String): Serializable

fun seService(_listService: ArrayList<Service>){
    listService.clear()
    listService = _listService
}

fun addService(p: Service){
    listService.add(p)
}

fun addMeeting(p: Meeting){
    listMeeting.add(p)
}

fun getAllService(): ArrayList<Service>{
    return listService
}


fun addEvent(p: Event){
    listEvent.add(p)
}

fun getAllEvent(): ArrayList<Event>{
    return listEvent
}

fun getAllMeeting(): ArrayList<Meeting>{
    return listMeeting
}

fun getAllQuote(): ArrayList<Quote>
{
    return listQuote
}

fun addQuote(q: Quote)
{
    listQuote.add(q)
}
