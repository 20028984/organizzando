package it.uniupo.organizzando

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.event_recycler_view_item.view.*


class EventAdapter (var events: ArrayList<Event>) : RecyclerView.Adapter<EventAdapter.EventViewHolder>(){


    fun updateEvent(newEvent: ArrayList<Event>) {
        events.clear()
        events.addAll(newEvent)
        notifyDataSetChanged()
    }


    private fun launchNextScreen(context: Context, Event: Event): Intent {
        val intent = Intent(context, EventActivity::class.java)
        intent.putExtra("Event", Event)
        return intent
    }


    override fun onCreateViewHolder( parent: ViewGroup, viewType: Int) : EventViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.event_recycler_view_item, parent, false)

        return EventViewHolder(cellForRow).listen { pos, _ ->
            val event:Event = events[pos]
            parent.context.startActivity(launchNextScreen(parent.context, event))
        }

    }


    override fun getItemCount() = events.size


    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.bind(events[position])
    }



    class EventViewHolder(view: View) : RecyclerView.ViewHolder(view) {


        private val name = view.nomeevento
        private val date = view.dataevento
        private val hour = view.oraevento


        fun bind(Event: Event) {

            val storage = FirebaseStorage.getInstance()

            name.text = Event.name
            date.text = Event.date
            hour.text= Event.hour
        }
    }


}

