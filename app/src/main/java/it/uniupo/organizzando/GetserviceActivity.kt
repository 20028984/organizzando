package it.uniupo.organizzando

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore


class GetserviceActivity : AppCompatActivity() {

    private val listService = ArrayList<Service>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_getservice)
        getService()
    }


    private fun getService(){
        val db = FirebaseFirestore.getInstance()

        db.collection("servizi")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    listService.add(Service(
                        name = document.data["nome"] as String,
                        description = document.data["descrizione"] as String,
                        image = document.data["immagine"] as String,
                        price= document.data["prezzo"] as Long,
                        category= document.data["categoria"] as String,
                        opening=  document.data["orario"] as String,
                        owner= document.data["proprietario"] as String,
                        place = document.data["luogo"] as String,
                        days = arrayListOf(document.data["giorni"].toString())
                    ))
                }
            }
            .addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents: ", exception)
            }

        seService(listService)
        startActivity(Intent(applicationContext, FornitoremainActivity::class.java))
    }

}