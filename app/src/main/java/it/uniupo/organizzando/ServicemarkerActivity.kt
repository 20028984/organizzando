package it.uniupo.organizzando

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.*
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import java.io.IOException
import java.util.*
import kotlin.math.log

internal class ServicemarkerActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private var buttonlist: ImageView? = null
    private var distance : Double = 0.0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_servicemarker)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        val event = intent.getSerializableExtra("evento") as? Event

        buttonlist = findViewById<View>(R.id.listButton) as ImageView

        buttonlist!!.setOnClickListener {
            val intent = Intent(this@ServicemarkerActivity, ChooseserviceActivity::class.java)
            intent.putExtra("evento", event)
            startActivity(intent)
        }


    }


    private val LOCATION_REQUEST_CODE = 101


    private fun requestPermission(permissionType: String, requestCode: Int) {
        ActivityCompat.requestPermissions(this, arrayOf(permissionType), requestCode
        )
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val permission = ContextCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_FINE_LOCATION)

        if (permission == PackageManager.PERMISSION_GRANTED) {
            mMap.isMyLocationEnabled = true
        } else {
            requestPermission(
                Manifest.permission.ACCESS_FINE_LOCATION,
                LOCATION_REQUEST_CODE)
        }

        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val criteria = Criteria()

        val location: Location? =
            locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false)!!)
        if (location != null) {
            mMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(
                        location.latitude,
                        location.longitude
                    ), 13f
                )
            )
            val cameraPosition = CameraPosition.Builder()
                .target(
                    LatLng(
                        location.latitude,
                        location.longitude
                    )
                ) // Sets the center of the map to location user
                .zoom(17f) // Sets the zoom
                .bearing(90f) // Sets the orientation of the camera to east
                .tilt(40f) // Sets the tilt of the camera to 30 degrees
                .build() // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        }


        val allServiceList: ArrayList<Service> = arrayListOf()

        allServiceList.clear()


        db().collection("servizi")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    allServiceList.add(Service(
                        document.data["nome"].toString(),
                        document.data["descrizione"].toString(),
                        document.data["immagine"].toString(),
                        document.data["prezzo"].toString().toLong(),
                        document.data["categoria"].toString(),
                        document.data["orario"].toString(),
                        document.data["proprietario"].toString(),
                        document.data["luogo"].toString(),
                        arrayListOf(document.data["giorni"].toString())
                    ))
                }
                var pos: LatLng?
                for (s in allServiceList)
                {
                    Log.d(ContentValues.TAG, "nome :${s.name}")
                    pos= getLocationFromAddress(s.place)
                    mMap.addMarker(pos.let {
                        MarkerOptions()
                            .position(it!!)
                            .title(s.name)
                            .snippet(s.description)
                    })
                }
            }
            .addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents: ", exception)
            }


        val mapSettings = mMap.uiSettings
        mapSettings.isZoomControlsEnabled = true
    }


    fun db(): FirebaseFirestore {
        return FirebaseFirestore.getInstance()
    }


    fun getLocationFromAddress(strAddress: String?): LatLng? {
        val coder = Geocoder(this)
        val address: List<Address>?
        var p1: GeoPoint? = null
        try {
            address = coder.getFromLocationName(strAddress, 5)
            if (address == null) {
                return null
            }
            val location: Address = address[0]
            location.latitude
            location.longitude
            p1 = GeoPoint(
                (location.latitude) as Double,
                (location.longitude) as Double
            )
            val lat: Double = p1.latitude
            val lng: Double = p1.longitude
            return LatLng(lat, lng)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }


}

