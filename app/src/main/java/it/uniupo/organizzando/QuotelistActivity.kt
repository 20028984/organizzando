package it.uniupo.organizzando

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_quotelist.*

class QuotelistActivity : AppCompatActivity() {
    private var auth: FirebaseAuth? = null
    private var tipoutente: String? = null
    private var back: ImageView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quotelist)

        val db = FirebaseFirestore.getInstance()


        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance()
        back = findViewById(R.id.iconaback1)

        val user = FirebaseAuth.getInstance().currentUser
        user?.let {
            val email = user.email.toString()
            val docRef = db.collection("utenti").document(email)
            docRef.get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        tipoutente = document.data?.get("tipo").toString()
                        Log.d(ContentValues.TAG, "DocumentSnapshot data: $tipoutente")
                    } else {
                        Log.d(ContentValues.TAG, "No such document")
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d(ContentValues.TAG, "get failed with ", exception)
                }
        }


        val allQuoteList: ArrayList<Quote> = getAllQuote()

        allQuoteList.clear()

        val user2 = FirebaseAuth.getInstance().currentUser?.email

        var userquoteList: ArrayList<Quote> = getAllQuote()

        userquoteList.clear()

        db.collection("preventivi")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    allQuoteList.add(
                        Quote(
                            document.data["codice"].toString(),
                            document.data["servizio"].toString(),
                            document.data["evento"].toString(),
                            document.data["organizzatore"].toString(),
                            document.data["fornitore"].toString(),
                            document.data["stato"].toString(),
                            document.data["metodo"].toString(),
                            document.data["modalita"].toString(),
                            document.data["conferma"].toString()
                        )
                    )
                }
                userquoteList=allQuoteList
                for(e in userquoteList)
                {
                    if(tipoutente=="1")
                    {
                        if(e.supplier!=user2) {
                            userquoteList.remove(e)
                        }
                    }else
                    {
                        if(e.manager!=user2) {
                            userquoteList.remove(e)
                        }
                    }
                }
                recyclerViewquote.layoutManager = GridLayoutManager(this, 1)
                recyclerViewquote.adapter = QuoteAdapter(quotes = userquoteList)
            }
            .addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents: ", exception)
            }


        back!!.setOnClickListener {
            val db2 = FirebaseFirestore.getInstance()
            val email = FirebaseAuth.getInstance().currentUser!!.email.toString()
            val docRef = db2.collection("utenti").document(email)
            docRef.get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        val ris = document.data?.get("tipo").toString()
                        Log.d(ContentValues.TAG, "DocumentSnapshot data: $ris")
                        if (ris == "2")
                        {
                            startIntent(OrganizzatoretruemainActivity::class.java)
                        }else
                        {
                            startIntent(FornitoretruemainActivity::class.java)
                        }
                    } else {
                        Log.d(ContentValues.TAG, "No such document")
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d(ContentValues.TAG, "get failed with ", exception)
                }
        }

    }

    override fun onResume() {
        super.onResume()
    }

    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }

    override fun onBackPressed() {
        val db = FirebaseFirestore.getInstance()
        val email = FirebaseAuth.getInstance().currentUser!!.email.toString()
        val docRef = db.collection("utenti").document(email)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    val ris = document.data?.get("tipo").toString()
                    Log.d(ContentValues.TAG, "DocumentSnapshot data: $ris")
                    if (ris == "2")
                    {
                        startIntent(OrganizzatoretruemainActivity::class.java)
                    }else
                    {
                        startIntent(FornitoretruemainActivity::class.java)
                    }
                } else {
                    Log.d(ContentValues.TAG, "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d(ContentValues.TAG, "get failed with ", exception)
            }
    }

}