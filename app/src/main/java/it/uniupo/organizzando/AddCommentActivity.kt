package it.uniupo.organizzando

import android.content.ContentValues
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.uniupo.organizzando.Adapter.CommentAdapter
import it.uniupo.organizzando.Adapter.PostAdapter
import it.uniupo.organizzando.Model.Comment
import it.uniupo.organizzando.Model.Post
import it.uniupo.organizzando.Model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_add_comment.*
import kotlinx.android.synthetic.main.activity_add_post.*
import kotlinx.android.synthetic.main.posts_layout.*

class AddCommentActivity : AppCompatActivity() {

    private var firebaseUser: FirebaseUser?=null
    private var commentAdapter:CommentAdapter?=null
    private var commentList:MutableList<Comment>?=null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_comment)

        val toolbar=findViewById<androidx.appcompat.widget.Toolbar>( R.id.comments_toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = "Comments"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener(View.OnClickListener {
            finish()
        })



        var recyclerView:RecyclerView?=null
        recyclerView=findViewById(R.id.recyclerview_comments)
        recyclerView.setHasFixedSize(true)
        val linearLayoutManager: LinearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager=linearLayoutManager

        commentList=ArrayList()
        commentAdapter= this.let { CommentAdapter(it,commentList as ArrayList<Comment>) }
        recyclerView.adapter=commentAdapter


        firebaseUser= FirebaseAuth.getInstance().currentUser

        val add_comment=findViewById<EditText>(R.id.add_comment)
        val post_comment=findViewById<TextView>(R.id.post_comment)
        val postid = intent.getStringExtra("POST_ID")
        val publisher = intent.getStringExtra("Publisher")
        val image = intent.getStringExtra("Image")
        Log.d(TAG,"post id: $postid")
        Log.d(TAG,"proprietario: $publisher")
        Log.d(TAG,"immagine: $image")



        readComments(postid!!)
        getPostImage(image!!)


        post_comment.setOnClickListener {
            if(add_comment.text.toString().equals(""))
            {
              Toast.makeText(this,"You can't send an empty comment",Toast.LENGTH_SHORT).show()
            }
            else
            {
                postComment(postid,publisher!!)
            }
        }

    }

    private fun postComment(postid:String,publisher:String) {

        val commentRef : DatabaseReference = FirebaseDatabase.getInstance("https://organizzando-default-rtdb.europe-west1.firebasedatabase.app").reference.child("Comment").child(postid)

        val commentMap = HashMap<String, Any>()
        commentMap["publisher"] = FirebaseAuth.getInstance().currentUser!!.email!!.split(".")[0]
        commentMap["comment"] = add_comment.text.toString()

        commentRef.push().setValue(commentMap)
        pushNotification(postid,publisher)
        add_comment.setText("")
        Toast.makeText(this, "posted!!", Toast.LENGTH_LONG).show()
    }


    private fun pushNotification(postid: String,publisher: String) {
        val db = FirebaseFirestore.getInstance()

        var stringa = "$publisher.com"

        db.collection("utenti").document(stringa)
            .get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    val ris2 = document.data?.get("mieicommenti").toString()
                    val ristoken = document.data?.get("token").toString()
                    val tipo =  document.data?.get("tipo").toString()
                    Log.d(ContentValues.TAG,"miei post: $ris2 token: $ristoken tipo: $tipo")
                    if(ris2=="si")
                    {
                        val user_token = ristoken
                        val notification_title = "Nuovo commento ad un tuo post!!"
                        val notification_des = "${FirebaseAuth.getInstance().currentUser!!.email!!} ha commentato un tuo post"
                        FCMMessages().sendMessageSingle(
                            this@AddCommentActivity,
                            user_token,
                            notification_title,
                            notification_des,
                            null
                        )
                    }

                } else {
                    Log.d(ContentValues.TAG, "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d(ContentValues.TAG, "get failed with ", exception)
            }


        var fatti : MutableList<Comment>? = null
        fatti=ArrayList()
        val ref: DatabaseReference =
            FirebaseDatabase.getInstance("https://organizzando-default-rtdb.europe-west1.firebasedatabase.app").reference.child("Comment").child(postid)

        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
            }

            override fun onDataChange(p0: DataSnapshot) {

                commentList?.clear()
                for (snapshot in p0.children) {
                    val cmnt: Comment? = snapshot.getValue(Comment::class.java)
                    commentList!!.add(cmnt!!)
                }
               for(c in commentList!!)
               {
                   if (!fatti.contains(c)){
                       db.collection("utenti").document(c.getPublisher()+".com")
                           .get()
                           .addOnSuccessListener { document ->
                               if (document != null) {
                                   val ris2 = document.data?.get("postcommentati").toString()
                                   val ristoken = document.data?.get("token").toString()
                                   val tipo =  document.data?.get("tipo").toString()
                                   Log.d(ContentValues.TAG,"miei post: $ris2 token: $ristoken tipo: $tipo")
                                   if(ris2=="si" && tipo=="3")
                                   {
                                       fatti.add(c)
                                       val user_token = ristoken
                                       val notification_title = "Nuovo commento ad un tuo post!!"
                                       val notification_des = "${FirebaseAuth.getInstance().currentUser!!.email!!} ha commentato un tuo post"
                                       FCMMessages().sendMessageSingle(
                                           this@AddCommentActivity,
                                           user_token,
                                           notification_title,
                                           notification_des,
                                           null
                                       )
                                   }

                               } else {
                                   Log.d(ContentValues.TAG, "No such document")
                               }
                           }
                           .addOnFailureListener { exception ->
                               Log.d(ContentValues.TAG, "get failed with ", exception)
                           }
                   }

               }
            }
        })


    }

    private fun readComments(postid: String) {
        val ref: DatabaseReference =
            FirebaseDatabase.getInstance("https://organizzando-default-rtdb.europe-west1.firebasedatabase.app").reference.child("Comment").child(postid)

        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
            }

            override fun onDataChange(p0: DataSnapshot) {

                commentList?.clear()
                for (snapshot in p0.children) {
                    val cmnt: Comment? = snapshot.getValue(Comment::class.java)
                    commentList!!.add(cmnt!!)
                }
                commentAdapter!!.notifyDataSetChanged()
            }
        })
    }

    private fun getPostImage(image:String){

        Picasso.get().load(image).placeholder(R.drawable.profile).into(post_image_comment)
    }
}