package it.uniupo.organizzando

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_organizzatoremain.*

class OrganizzatoremainActivity : AppCompatActivity() {
    private var btnadd: ImageView? = null
    private var auth: FirebaseAuth? = null
    private var btnback: ImageView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_organizzatoremain)


        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance()
        btnadd = findViewById<View>(R.id.add) as ImageView
        btnback = findViewById<View>(R.id.iconaback5) as ImageView



        val allEventList: ArrayList<Event> = getAllEvent()

        allEventList.clear()

        val db = FirebaseFirestore.getInstance()

        val user = FirebaseAuth.getInstance().currentUser?.email


        var usereventList: ArrayList<Event> = getAllEvent()

        usereventList.clear()

        db.collection("eventi")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    allEventList.add(Event(
                        document.data["nome"].toString(),
                        document.data["data"].toString(),
                        document.data["orario"].toString(),
                        document.data["budget"].toString().toLong(),
                        document.data["luogo"].toString(),
                        document.data["proprietario"].toString(),
                        document.data["categoria"].toString(),
                        document.data["budgetattuale"].toString().toLong(),
                        arrayListOf(document.data["servizi"].toString()),
                        document.data["codiceinvito"].toString(),
                        arrayListOf(document.data["invitati"].toString()),
                        arrayListOf(document.data["tipiservizio"].toString())
                    ))
                }
                usereventList=allEventList
                for(e in usereventList)
                {
                    if(e.owner!=user)
                    {
                        usereventList.remove(e)
                    }
                }
                recyclerViewEventi.layoutManager = GridLayoutManager(this, 1)
                recyclerViewEventi.adapter = EventAdapter(events = usereventList)
            }
            .addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents: ", exception)
            }



        btnadd!!.setOnClickListener(View.OnClickListener {
            startIntent(AddeventActivity::class.java)
        })

        btnback!!.setOnClickListener(View.OnClickListener {
            startIntent(OrganizzatoretruemainActivity::class.java)
        })

    }

    override fun onResume() {
        super.onResume()
    }

    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }

    override fun onBackPressed() {
        startActivity(Intent(this, OrganizzatoretruemainActivity::class.java))
    }
}