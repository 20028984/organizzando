package it.uniupo.organizzando


import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.messaging.FirebaseMessaging
import it.uniupo.organizzando.*
import kotlinx.android.synthetic.main.activity_addevent.*
import kotlinx.android.synthetic.main.activity_guesteventlist.*


class LoginActivity : AppCompatActivity() {
    private var inputEmail: EditText? = null
    private var inputPassword: EditText? = null
    private var auth: FirebaseAuth? = null
    private var progressBar: ProgressBar? = null
    private var btnSignup: Button? = null
    private var btnLogin: Button? = null
    private var btnReset: Button? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val db = FirebaseFirestore.getInstance()
        val TAG = SignupActivity::class.java.simpleName

        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance()


        // set the view now
        setContentView(R.layout.activity_login)
        inputEmail = findViewById<View>(R.id.email) as EditText
        inputPassword = findViewById<View>(R.id.password) as EditText
        progressBar = findViewById<View>(R.id.progressBar) as ProgressBar
        btnSignup = findViewById<View>(R.id.btn_signup) as Button
        btnLogin = findViewById<View>(R.id.btn_login) as Button
        btnReset = findViewById<View>(R.id.btn_reset_password) as Button


        if (auth!!.currentUser != null) {
            val user = FirebaseAuth.getInstance().currentUser
            val email = user?.email.toString()
            val docRef = db.collection("utenti").document(email)
            docRef.get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        val ris = document.data?.get("tipo").toString()
                        Log.d(TAG, "DocumentSnapshot data: $ris")
                        if (ris == "1")
                        {
                            startIntent(FornitoretruemainActivity::class.java)
                        }else if (ris == "2")
                        {
                            startIntent(OrganizzatoretruemainActivity::class.java)
                        }else
                        {
                            startIntent(GuesteventlistActivity::class.java)
                        }
                    } else {
                        Log.d(TAG, "No such document")
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d(TAG, "get failed with ", exception)
                }

            finish()
        }

        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance()
        btnSignup!!.setOnClickListener {
            startActivity(
                Intent(
                    this@LoginActivity,
                    SignupActivity::class.java
                )
            )
        }
        btnReset!!.setOnClickListener {
            startActivity(
                Intent(
                    this@LoginActivity,
                    ResetPasswordActivity::class.java
                )
            )
        }
        btnLogin!!.setOnClickListener(View.OnClickListener {
            val email = inputEmail!!.text.toString()
            val password = inputPassword!!.text.toString()
            if (TextUtils.isEmpty(email)) {
                Toast.makeText(applicationContext, "Enter email address!", Toast.LENGTH_SHORT)
                    .show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(password)) {
                Toast.makeText(applicationContext, "Enter password!", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            progressBar!!.visibility = View.VISIBLE


            //authenticate user
            auth!!.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(
                    this@LoginActivity
                ) { task ->
                    // If sign in fails, display a message to the user. If sign in succeeds
                    // the auth state listener will be notified and logic to handle the
                    // signed in user can be handled in the listener.
                    progressBar!!.visibility = View.GONE
                    if (!task.isSuccessful) {
                        // there was an error
                        if (password.length < 6) {
                            inputPassword!!.error = getString(R.string.minimum_password)
                        } else {
                            Toast.makeText(
                                this@LoginActivity,
                                getString(R.string.auth_failed),
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    } else {
                        val docRef = db.collection("utenti").document(email)
                        docRef.get()
                            .addOnSuccessListener { document ->
                                if (document != null) {
                                    FirebaseMessaging.getInstance().token.addOnCompleteListener(
                                        OnCompleteListener { task ->
                                        if (!task.isSuccessful) {
                                            Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                                            return@OnCompleteListener
                                        }

                                        // Get new FCM registration token
                                        val token = task.result

                                        // Log and toast
                                        db.collection("utenti").document(email)
                                            .update("token", token.toString())
                                            .addOnSuccessListener { Log.d(ContentValues.TAG, "DocumentSnapshot successfully written!") }
                                            .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }

                                        Log.d(TAG, "token: $token")
                                        FirebaseMessaging.getInstance().subscribeToTopic(email)

                                    })
                                    val ris = document.data?.get("tipo").toString()
                                    Log.d(TAG, "DocumentSnapshot data: $ris")
                                    if (ris == "1")
                                    {
                                        startIntent(FornitoretruemainActivity::class.java)
                                    }else if (ris == "2")
                                    {
                                        startIntent(OrganizzatoretruemainActivity::class.java)
                                    }else
                                    {
                                        startIntent(GuesteventlistActivity::class.java)
                                    }
                                } else {
                                    Log.d(TAG, "No such document")
                                }
                            }
                            .addOnFailureListener { exception ->
                                Log.d(TAG, "get failed with ", exception)
                            }
                        finish()
                    }
                }
        })
    }


    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }
}
