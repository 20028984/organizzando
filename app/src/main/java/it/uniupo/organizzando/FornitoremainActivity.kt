package it.uniupo.organizzando

import android.content.ContentValues
import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_fornitoremain.*

class FornitoremainActivity : AppCompatActivity() {
    private var btnback: ImageView? = null
    private var btnadd: ImageView? = null
    private var auth: FirebaseAuth? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fornitoremain)


        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance()
        btnadd = findViewById<View>(R.id.add) as ImageView
        btnback = findViewById<View>(R.id.iconaback4) as ImageView



        val allServiceList: ArrayList<Service> = getAllService()

        allServiceList.clear()

        val db = FirebaseFirestore.getInstance()

        val user = FirebaseAuth.getInstance().currentUser?.email


        var userserviceList: ArrayList<Service> = getAllService()

        userserviceList.clear()


        db.collection("servizi")
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    allServiceList.add(Service(
                        document.data["nome"].toString(),
                        document.data["descrizione"].toString(),
                        document.data["immagine"].toString(),
                        document.data["prezzo"].toString().toLong(),
                        document.data["categoria"].toString(),
                        document.data["orario"].toString(),
                        document.data["proprietario"].toString(),
                        document.data["luogo"].toString(),
                        arrayListOf(document.data["giorni"].toString()),
                    ))
                }
                userserviceList=allServiceList
                for(s in userserviceList)
                {
                    if(s.owner!=user)
                    {
                        userserviceList.remove(s)
                    }
                }
                recyclerViewServizi.layoutManager = GridLayoutManager(this, 2)
                recyclerViewServizi.adapter = ServiceAdapter(services = userserviceList)
            }
            .addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents: ", exception)
            }


        btnadd!!.setOnClickListener(View.OnClickListener {
            startIntent(AddserviceActivity::class.java)
        })

        btnback!!.setOnClickListener(View.OnClickListener {
            startIntent(FornitoretruemainActivity::class.java)
        })



    }

    override fun onBackPressed() {
        startActivity(Intent(this, FornitoretruemainActivity::class.java))
    }

    override fun onResume() {
        super.onResume()
    }

    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }
}