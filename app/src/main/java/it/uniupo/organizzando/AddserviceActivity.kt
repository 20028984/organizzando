package it.uniupo.organizzando

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.ContentValues.TAG
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import it.uniupo.organizzando.fragment.TimePickerFragment1
import it.uniupo.organizzando.fragment.TimePickerFragment2
import kotlinx.android.synthetic.main.activity_addservice.*
import java.io.ByteArrayOutputStream

class AddserviceActivity : AppCompatActivity() {
    private val PERMISSIONCODE = 1000;
    private val IMAGE_CAPTURE_CODE = 1001
    var image_uri: Uri? = null
    private var buttonupload: ImageView? = null
    private var buttonsubmit: Button? = null
    private var orario1text: TextView? = null
    private var orario2text: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addservice)

        buttonupload = findViewById<View>(R.id.anteprima) as ImageView
        buttonsubmit = findViewById<View>(R.id.btn_submit) as Button
        orario1text = findViewById<View>(R.id.orarioinizio) as TextView
        orario2text = findViewById<View>(R.id.orariofine) as TextView
        val storage = FirebaseStorage.getInstance()

        orario1text!!.setOnClickListener{
            TimePickerFragment1().show(supportFragmentManager, "timePicker")
        }
        orario2text!!.setOnClickListener{
            TimePickerFragment2().show(supportFragmentManager, "timePicker")
        }


        buttonupload!!.setOnClickListener {

            buttonsubmit!!.isEnabled = true
            buttonsubmit!!.isClickable = true
            buttonsubmit!!.setBackgroundColor(Color.GREEN)

            if (checkSelfPermission(Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED ||
                checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_DENIED
            ) {
                //permission was not enabled
                val permission = arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                //show popup to request permission
                requestPermissions(permission, PERMISSIONCODE)
            } else {
                //permission already granted
                openCamera()
            }
        }

        buttonsubmit!!.setOnClickListener{

            if(nome.text.isEmpty()){
                Toast.makeText(this, "Devi inserire un nome!", Toast.LENGTH_SHORT).show()
            }
            else if(descrizione.text.isEmpty()){
                Toast.makeText(this, "Devi inserire una descrizione!", Toast.LENGTH_SHORT).show()
            }
            else if(prezzo.text.isEmpty()){
                Toast.makeText(this, "Devi inserire un prezzo!", Toast.LENGTH_SHORT).show()
            }
            else if (orarioinizio.text=="Tocca per aggiungere l'orario di apertura")
            {
                Toast.makeText(this, "Devi aggiungere un orario di apertura!", Toast.LENGTH_SHORT).show()
            }
            else if (orariofine.text=="Tocca per aggiungere l'orario di chiusura")
            {
                Toast.makeText(this, "Devi aggiungere un orario di chiusura!", Toast.LENGTH_SHORT).show()
            }else {


                var downloadUri = ""

                val storageRef = storage.reference

                val ImagesRef = storageRef.child("images/${nome.text}.jpg")


                anteprima.isDrawingCacheEnabled = true
                anteprima.buildDrawingCache()
                val bitmap = (anteprima.drawable as BitmapDrawable).bitmap
                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val data = baos.toByteArray()

                val uploadTask = ImagesRef.putBytes(data)

                val urlTask = uploadTask.continueWithTask { task ->
                    if (!task.isSuccessful) {
                        task.exception?.let {
                            throw it
                        }
                    }
                    ImagesRef.downloadUrl
                }.addOnCompleteListener { task ->
                    if (task.isSuccessful) {

                        downloadUri = task.result.toString()
                        val user = FirebaseAuth.getInstance().currentUser!!.email
                        val vuoto = ArrayList<String>()

                        val intent =
                            Intent(this@AddserviceActivity, Addservice2Activity::class.java)
                        val p = Service(
                            nome.text.toString(),
                            descrizione.text.toString(),
                            downloadUri,
                            prezzo.text.toString().toLong(),
                            spinnercategorie.selectedItem.toString(),
                            orarioinizio.text.toString() + "-" + orariofine.text.toString(),
                            user.toString(),
                            "nessuno",
                            vuoto
                        )
                        intent.putExtra("servizio", p)
                        startActivity(intent)


                    } else {
                        Toast.makeText(applicationContext, "Errore!", Toast.LENGTH_SHORT).show()
                    }
                }


            }
        }


    }


    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        image_uri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        //called when user presses ALLOW or DENY from Permission Request Popup
        when (requestCode) {
            PERMISSIONCODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    //permission from popup was granted
                    openCamera()
                } else {
                    //permission from popup was denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //called when image was captured from camera intent
        if (resultCode == Activity.RESULT_OK) {
            //set image captured to image view
            anteprima.setImageURI(image_uri)
        }
    }

    fun db(): FirebaseFirestore {
        return FirebaseFirestore.getInstance()
    }

    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }

    override fun onBackPressed() {
        startActivity(Intent(this, FornitoremainActivity::class.java))
    }

}