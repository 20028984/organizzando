package it.uniupo.organizzando


import android.app.Activity
import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import it.uniupo.organizzando.fragment.DatePickerFragment3
import it.uniupo.organizzando.fragment.TimePickerFragment5
import kotlinx.android.synthetic.main.activity_addevent.*
import kotlinx.android.synthetic.main.activity_modev.*

class ModevActivity : AppCompatActivity() {
    private var buttonsubmit: Button? = null
    private var orarioev: TextView? = null
    private var dataev: TextView? = null
    private var buttondelete: Button? = null
    private var luogoev: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_modev)

        buttonsubmit = findViewById<View>(R.id.btn_submitmodev) as Button
        buttondelete = findViewById<View>(R.id.btn_deleteev) as Button
        orarioev = findViewById<View>(R.id.oraev2) as TextView
        dataev = findViewById<View>(R.id.dataev2) as TextView
        luogoev = findViewById<View>(R.id.luogo2) as TextView

        val event = intent.getSerializableExtra("evento") as? Event
        var stringa= "Budget totale: "+event!!.budget.toString()

        nomeevento2.text = event.name
        budget2.text = stringa
        stringa =  event.place + "(tocca per modificare)"
        luogo2.text= stringa
        stringa = event.date + " (tocca per modificare)"
        dataev2.text = stringa
        stringa = event.hour + " (tocca per modificare)"
        oraev2.text = stringa
        stringa= "Categoria: "+event.category
        spinnercategorieevento2.text= stringa

        luogoev!!.setOnClickListener {
            val intent = Intent(this@ModevActivity, MapsActivity::class.java)
            startActivityForResult(intent,1)
        }


        dataev!!.setOnClickListener{
            DatePickerFragment3().show(supportFragmentManager, "datePicker")
        }
        orarioev!!.setOnClickListener{
            TimePickerFragment5().show(supportFragmentManager, "timePicker")
        }
        

        buttonsubmit!!.setOnClickListener{


            val datafinale: String
            var orafinale = ""
            if(dataev2.text==(event.date + " (tocca per modificare)"))
            {
                datafinale=event.date
            }else
            {
                datafinale=dataev2.text.toString()
            }
            if(oraev2.text==(event.hour + " (tocca per modificare)"))
            {
                orafinale=event.date
            }else
            {
                orafinale=dataev2.text.toString()
            }

                    val user2 = FirebaseAuth.getInstance().currentUser?.email
                    val evento = hashMapOf(
                        "nome" to nomeevento2.text.toString(),
                        "data" to datafinale,
                        "orario" to orafinale,
                        "budget" to event.budget,
                        "luogo" to luogo2.text.toString(),
                        "proprietario" to event.owner,
                        "categoria" to spinnercategorieevento2.toString(),
                        "budgetattuale" to event.actualbudget,
                        "servizi" to event.services
                    )

                    db().collection("eventi").document(event.name)
                        .set(evento)
                        .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully written!") }
                        .addOnFailureListener { e -> Log.w(TAG, "Error writing document", e) }

                    val user = FirebaseAuth.getInstance().currentUser

                    val p = Event(nomeevento2.text.toString(),datafinale,orafinale,event.budget,luogo2.toString(),event.owner,spinnercategorieevento2.toString(),event.actualbudget,event.services,event.invitecode,event.guests,event.typeserv)


                    addEvent(p)


                    val intent = Intent(this@ModevActivity, OrganizzatoremainActivity::class.java)
                    startActivity(intent)

        }

        buttondelete!!.setOnClickListener(){

            val builder = AlertDialog.Builder(this, R.style.AlertDialog)
            builder.setTitle("Conferma: ")
            builder.setMessage("Sei sicuro di voler cancellare l'evento?")
            builder.setPositiveButton(android.R.string.yes) { dialog, which ->


                db().collection("eventi").document(event.name)
                    .delete()
                    .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully deleted!") }
                    .addOnFailureListener { e -> Log.w(TAG, "Error deleting document", e) }


                val intent = Intent(this@ModevActivity, OrganizzatoremainActivity::class.java)
                startActivity(intent)
            }

            builder.setNegativeButton("No") { dialog, which ->
                Toast.makeText(
                    applicationContext,
                    "operazione annullata!", Toast.LENGTH_SHORT
                ).show()
            }
            builder.show()
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val returnString = data!!.getStringExtra("result")
            luogo2.text = returnString
        }
    }


    fun db(): FirebaseFirestore {
        return FirebaseFirestore.getInstance()
    }

    override fun onBackPressed() {
        startActivity(Intent(this, OrganizzatoremainActivity::class.java))
    }

}