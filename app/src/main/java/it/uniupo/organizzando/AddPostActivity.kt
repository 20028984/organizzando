package it.uniupo.organizzando

import android.app.Activity
import android.app.ProgressDialog
import android.content.ContentValues
import android.content.ContentValues.TAG
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.StorageTask
import com.google.firebase.storage.UploadTask
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import it.uniupo.organizzando.FeedActivity
import it.uniupo.organizzando.R
import kotlinx.android.synthetic.main.activity_add_comment.*
import kotlinx.android.synthetic.main.activity_add_post.*

class AddPostActivity : AppCompatActivity() {

    private  var myUrl=""
    private  var imageUri: Uri?=null
    private  var storagePostPictureRef: StorageReference?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_post)

        storagePostPictureRef= FirebaseStorage.getInstance().reference.child("Post Picture")
        val event = intent.getSerializableExtra("Event") as? Event

        dont_post_picture.setOnClickListener {
            val intent=Intent(this@AddPostActivity,FeedActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }

        post_picture.setOnClickListener {
            uploadPost(event!!)
        }

        picture_to_be_posted.setOnClickListener {
            CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1,1)
                .start(this@AddPostActivity)
        }

        CropImage.activity()
            .setGuidelines(CropImageView.Guidelines.ON)
            .setAspectRatio(1,1)
            .start(this@AddPostActivity)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK) {
                imageUri= result.uri
                picture_to_be_posted.setImageURI(imageUri)
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
            }
        }
    }

    private fun uploadPost(event: Event) {
        when
        {
            imageUri == null -> Toast.makeText(this, "Devi inserire una immagine!", Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(write_post.text.toString()) -> Toast.makeText(this, "Inserisci il testo!", Toast.LENGTH_LONG).show()

            else -> {
                val progressDialog = ProgressDialog(this)
                progressDialog.setTitle("Posting")
                progressDialog.setMessage("Attendi, creazione post in corso...")
                progressDialog.show()

                val fileRef = storagePostPictureRef!!.child(System.currentTimeMillis().toString()+ ".jpg")

                var uploadTask: StorageTask<*>
                uploadTask = fileRef.putFile(imageUri!!)

                uploadTask.continueWithTask(Continuation <UploadTask.TaskSnapshot, Task<Uri>>{ task ->
                    if (!task.isSuccessful)
                    {
                        task.exception?.let {
                            throw it
                            progressDialog.dismiss()
                        }
                    }
                    return@Continuation fileRef.downloadUrl
                }).addOnCompleteListener (OnCompleteListener<Uri> { task ->
                    if (task.isSuccessful)
                    {
                        val downloadUrl = task.result
                        myUrl = downloadUrl.toString()


                        val ref = FirebaseDatabase.getInstance("https://organizzando-default-rtdb.europe-west1.firebasedatabase.app").reference.child("Posts")
                        val postid=ref.push().key

                        val postMap = HashMap<String, Any>()

                        postMap["postid"] = postid!!
                        postMap["caption"] = write_post.text.toString()
                        postMap["publisher"] = FirebaseAuth.getInstance().currentUser!!.email!!.split(".")[0]
                        postMap["postimage"] = myUrl
                        postMap["event"] = event.name

                        ref.child(event.name).child(postid).updateChildren(postMap)


                        val commentRef=FirebaseDatabase.getInstance("https://organizzando-default-rtdb.europe-west1.firebasedatabase.app").reference.child("Comment").child(postid)
                        val commentMap = HashMap<String, Any>()
                        commentMap["publisher"] = FirebaseAuth.getInstance().currentUser!!.email!!.split(".")[0]
                        commentMap["comment"] =  write_post.text.toString()


                        commentRef.push().setValue(commentMap)

                        Log.d(TAG,"evento: ${event.name} proprietario: ${event.owner}")

                        val db = FirebaseFirestore.getInstance()

                        val docRef = db.collection("utenti").document(event.owner)
                        docRef.get()
                            .addOnSuccessListener { document ->
                                if (document != null) {
                                    val ris = document.data?.get("token").toString()
                                    Log.d(TAG,"token proprietario: $ris")
                                    val user_token = ris
                                    val notification_title = "Nuovo post pubblicato nel tuo evento!!"
                                    val notification_des = "${FirebaseAuth.getInstance().currentUser!!.email!!} ha creato un nuovo post sull'evento ${event.name}"
                                    FCMMessages().sendMessageSingle(
                                        this@AddPostActivity,
                                        user_token,
                                        notification_title,
                                        notification_des,
                                        null
                                    )

                                } else {
                                    Log.d(ContentValues.TAG, "No such document")
                                }
                            }
                            .addOnFailureListener { exception ->
                                Log.d(ContentValues.TAG, "get failed with ", exception)
                            }



                            db.collection("eventi").document(event.name)
                                .get()
                                .addOnSuccessListener { document2 ->
                                    if (document2 != null) {
                                        var arrayris = arrayListOf(document2.data!!["invitati"].toString()).toString()
                                        Log.d(TAG,"invitati prima: ${arrayris.toString()}")

                                        arrayris= arrayris.replace("[","")
                                        arrayris= arrayris.replace("]","")
                                        arrayris= arrayris.replace(" ","")

                                        val arrayfinal= arrayris.split(",")

                                        Log.d(TAG,"invitati dopo: $arrayfinal")
                                        for (r in arrayfinal){
                                            Log.d(TAG,"utente: $r")
                                            db.collection("utenti").document(r)
                                                .get()
                                                .addOnSuccessListener { document3 ->
                                                    if (document3 != null) {
                                                        val ris2 = document3.data?.get("nuovipost").toString()
                                                        val ristoken = document3.data?.get("token").toString()
                                                        Log.d(TAG,"nuovi post: $ris2 token: $ristoken")
                                                        if(ris2=="si")
                                                        {
                                                            val user_token = ristoken
                                                            val notification_title = "Nuovo post pubblicato!!"
                                                            val notification_des = "${FirebaseAuth.getInstance().currentUser!!.email!!} ha creato un nuovo post sull'evento ${event.name}"
                                                            FCMMessages().sendMessageSingle(
                                                                this@AddPostActivity,
                                                                user_token,
                                                                notification_title,
                                                                notification_des,
                                                                null
                                                            )
                                                        }

                                                    } else {
                                                        Log.d(ContentValues.TAG, "No such document")
                                                    }
                                                }
                                                .addOnFailureListener { exception ->
                                                    Log.d(ContentValues.TAG, "get failed with ", exception)
                                                }
                                        }

                                    } else {
                                        Log.d(ContentValues.TAG, "No such document")
                                    }
                                }
                                .addOnFailureListener { exception ->
                                    Log.d(ContentValues.TAG, "get failed with ", exception)
                                }



                        Toast.makeText(this, "Nuovo post creato correttamente!", Toast.LENGTH_LONG).show()

                        val intent = Intent(this@AddPostActivity, FeedActivity::class.java)

                        intent.putExtra("Event", event)
                        startActivity(intent)
                        finish()
                        progressDialog.dismiss()
                    }
                    else
                    {
                        progressDialog.dismiss()
                    }
                })
            }
        }
    }
 }