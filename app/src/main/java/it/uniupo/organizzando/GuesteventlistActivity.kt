package it.uniupo.organizzando

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_guesteventlist.*

class GuesteventlistActivity : AppCompatActivity() {
    private var btnadd: ImageView? = null
    private var btnlogout: ImageView? = null
    private var auth: FirebaseAuth? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guesteventlist)


        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance()
        btnadd = findViewById<View>(R.id.addevinv) as ImageView
        btnlogout = findViewById<View>(R.id.logout) as ImageView


        val allEventList: ArrayList<Event> = getAllEvent()

        allEventList.clear()

        val db = FirebaseFirestore.getInstance()

        val user = FirebaseAuth.getInstance().currentUser?.email


        db.collection("eventi")
            .whereArrayContains("invitati", user!!)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    allEventList.add(Event(
                        document.data["nome"].toString(),
                        document.data["data"].toString(),
                        document.data["orario"].toString(),
                        document.data["budget"].toString().toLong(),
                        document.data["luogo"].toString(),
                        document.data["proprietario"].toString(),
                        document.data["categoria"].toString(),
                        document.data["budgetattuale"].toString().toLong(),
                        arrayListOf(document.data["servizi"].toString()),
                        document.data["codiceinvito"].toString(),
                        arrayListOf(document.data["invitati"].toString()),
                        arrayListOf(document.data["tipiservizio"].toString())
                    ))
                }

                recyclerViewEventiinv.layoutManager = GridLayoutManager(this, 1)
                recyclerViewEventiinv.adapter = EventAdapter(events = allEventList)
            }
            .addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents: ", exception)
            }



        btnadd!!.setOnClickListener(View.OnClickListener {
            startIntent(InvitatomainActivity::class.java)
        })

        btnlogout!!.setOnClickListener(View.OnClickListener {
            startIntent(OptionsAccountActivity::class.java)
        })

    }

    override fun onResume() {
        super.onResume()
    }

    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }


}