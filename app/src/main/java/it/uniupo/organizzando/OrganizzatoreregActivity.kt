package it.uniupo.organizzando

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class OrganizzatoreregActivity: AppCompatActivity() {
    private var inputnome: EditText? = null
    private var inputcognome: EditText? = null
    private var inputdata: EditText? = null
    private var inputluogonasc: EditText? = null
    private var inputcodfisc: EditText? = null
    private var inputind: EditText? = null
    private var inputcitta: EditText? = null
    private var inputcap: EditText? = null
    private var inputiban: EditText? = null
    private var btnregister: Button? = null
    private var progressBar: ProgressBar? = null
    private var auth: FirebaseAuth? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_organizzatorereg)
        val db = FirebaseFirestore.getInstance()
        val TAG = SignupActivity::class.java.simpleName



        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance()
        inputnome = findViewById<View>(R.id.nome) as EditText
        inputcognome = findViewById<View>(R.id.cognome) as EditText
        inputdata = findViewById<View>(R.id.datanascita) as EditText
        inputluogonasc = findViewById<View>(R.id.luogonascita) as EditText
        inputcodfisc = findViewById<View>(R.id.codfisc) as EditText
        inputcitta = findViewById<View>(R.id.citta) as EditText
        inputind = findViewById<View>(R.id.indirizzo) as EditText
        inputcap = findViewById<View>(R.id.cap) as EditText
        inputiban = findViewById<View>(R.id.iban) as EditText
        btnregister = findViewById<View>(R.id.register) as Button
        progressBar = findViewById<View>(R.id.progressBar) as ProgressBar

        val intent = intent
        val email = intent.getStringExtra("email")!!
        val password = intent.getStringExtra("password")!!

        btnregister!!.setOnClickListener(View.OnClickListener {
            val nome = inputnome!!.text.toString().trim { it <= ' ' }
            val cognome = inputcognome!!.text.toString().trim { it <= ' ' }
            val datanasc = inputdata!!.text.toString().trim { it <= ' ' }
            val luogonasc = inputluogonasc!!.text.toString().trim { it <= ' ' }
            val indirizzo = inputind!!.text.toString().trim { it <= ' ' }
            val citta = inputcitta!!.text.toString().trim { it <= ' ' }
            val codfisc= inputcodfisc!!.text.toString().trim { it <= ' ' }
            val cap= inputcap!!.text.toString().trim { it <= ' ' }
            val iban = inputiban!!.text.toString().trim { it <= ' ' }
            if (TextUtils.isEmpty(nome)) {
                Toast.makeText(applicationContext, "Inserisci il nome!", Toast.LENGTH_SHORT)
                    .show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(cognome)) {
                Toast.makeText(applicationContext, "Inserisci il cognome!", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(datanasc)) {
                Toast.makeText(applicationContext, "Inserisci la data di nascita!", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(luogonasc)) {
                Toast.makeText(applicationContext, "Inserisci il tuo luogo di nascita!", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(indirizzo)) {
                Toast.makeText(applicationContext, "Inserisci il tuo indirizzo!", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(cap)) {
                Toast.makeText(applicationContext, "Inserisci il CAP della tua città di residenza!", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(citta)) {
                Toast.makeText(applicationContext, "Inserisci la tua città di residenza!", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(codfisc)) {
                Toast.makeText(applicationContext, "Inserisci il tuo codice fiscale!", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            if (TextUtils.isEmpty(iban)) {
                Toast.makeText(applicationContext, "Inserisci l'IBAN!", Toast.LENGTH_SHORT).show()
                return@OnClickListener
            }
            progressBar!!.visibility = View.VISIBLE
            //create user
            auth!!.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(
                    this@OrganizzatoreregActivity
                ) { task ->
                    Toast.makeText(
                        this@OrganizzatoreregActivity,
                        "Utente creato con successo!",
                        Toast.LENGTH_SHORT
                    ).show()
                    progressBar!!.visibility = View.GONE
                    // If sign in fails, display a message to the user. If sign in succeeds
                    // the auth state listener will be notified and logic to handle the
                    // signed in user can be handled in the listener.
                    if (!task.isSuccessful) {
                        Toast.makeText(
                            this@OrganizzatoreregActivity, "Authentication failed." + task.exception,
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {

                        val dati = hashMapOf(
                            "tipo" to 2,
                            "nome" to nome,
                            "cognome" to cognome,
                            "datanasc" to datanasc,
                            "luogonasc" to luogonasc,
                            "indirizzo" to indirizzo,
                            "cap" to cap,
                            "codfisc" to codfisc,
                            "citta" to citta,
                            "iban" to iban
                        )

                        db.collection("utenti").document(email)
                            .set(dati)
                            .addOnSuccessListener { documentReference ->
                                Log.d(TAG, "Document successful written!")
                            }
                            .addOnFailureListener { e ->
                                Log.w(TAG, "Error adding document", e)
                            }

                        startIntent(LoginActivity::class.java)

                        finish()
                    }
                }
        })
    }

    override fun onResume() {
        super.onResume()
        progressBar!!.visibility = View.GONE
    }

    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }
}