package it.uniupo.organizzando

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.service_recycler_view_item.view.*
import kotlinx.android.synthetic.main.service_recycler_view_item.view.nome
import kotlinx.android.synthetic.main.service_recycler_view_item.view.previewIcon


class ServiceAdapter2 (var services: ArrayList<Service>, var event: Event) : RecyclerView.Adapter<ServiceAdapter2.ServiceViewHolder>(){


    fun updateService(newService: ArrayList<Service>) {
        services.clear()
        services.addAll(newService)
        notifyDataSetChanged()
    }


    private fun launchNextScreen(context: Context, Service: Service): Intent {
        val intent = Intent(context, ServiceActivity::class.java)
        intent.putExtra("Service", Service)
        intent.putExtra("Event", event)
        return intent
    }


    override fun onCreateViewHolder( parent: ViewGroup, viewType: Int) : ServiceViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.service_recycler_view_item, parent, false)

        return ServiceViewHolder(cellForRow).listen { pos, _ ->
            val service:Service = services[pos]
            parent.context.startActivity(launchNextScreen(parent.context, service))
        }

    }


    override fun getItemCount() = services.size


    override fun onBindViewHolder(holder: ServiceViewHolder, position: Int) {
        holder.bind(services[position])
    }



    class ServiceViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val preview = view.previewIcon
        private val name = view.nome
        private val category = view.categoria




        fun bind(Service: Service) {

            val storage = FirebaseStorage.getInstance()

            val gsReference = storage.getReferenceFromUrl(Service.image)



            gsReference.downloadUrl.addOnSuccessListener {Uri->

                val imageURL = Uri.toString()

                Picasso.get().load(imageURL).into(preview)

            }

            name.text = Service.name
            category.text = Service.category
        }
    }

}

