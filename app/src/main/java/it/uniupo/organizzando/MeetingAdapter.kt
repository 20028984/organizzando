package it.uniupo.organizzando

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.meeting_recycler_view_item.view.*


class MeetingAdapter (var meetings: ArrayList<Meeting>) : RecyclerView.Adapter<MeetingAdapter.MeetingViewHolder>(){


    fun updatemeeting(newmeeting: ArrayList<Meeting>) {
        meetings.clear()
        meetings.addAll(newmeeting)
        notifyDataSetChanged()
    }


    private fun launchNextScreen(context: Context, meeting: Meeting): Intent {
        val intent = Intent(context, MeetingActivity::class.java)
        intent.putExtra("Meeting", meeting)
        return intent
    }


    override fun onCreateViewHolder( parent: ViewGroup, viewType: Int) : MeetingViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val cellForRow = layoutInflater.inflate(R.layout.meeting_recycler_view_item, parent, false)

        return MeetingViewHolder(cellForRow).listen { pos, _ ->
            val meeting:Meeting = meetings[pos]
            parent.context.startActivity(launchNextScreen(parent.context, meeting))
        }

    }


    override fun getItemCount() = meetings.size


    override fun onBindViewHolder(holder: MeetingViewHolder, position: Int) {
        holder.bind(meetings[position])
    }



    class MeetingViewHolder(view: View) : RecyclerView.ViewHolder(view) {


        private val name = view.nomeapp
        private val date = view.dataapp
        private val hour = view.oraapp
        private val status = view.stato


        fun bind(meeting: Meeting) {


            name.text = meeting.codice
            date.text = meeting.data
            hour.text= meeting.ora
            status.text = meeting.status
        }
    }


}

