package it.uniupo.organizzando

import android.app.AlertDialog
import android.content.ContentValues
import android.content.ContentValues.TAG
import android.content.DialogInterface
import android.content.Intent
import android.nfc.Tag
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Spinner
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.*
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_quote.*
import kotlinx.android.synthetic.main.price_review_title_section.*
import java.sql.Timestamp


class QuoteActivity : AppCompatActivity() {

    private var buttonaccept: Button? = null
    private var buttondelete: Button? = null
    private var buttonconfirm: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quote)
        buttonaccept= findViewById<View>(R.id.btnacceptprev) as Button
        buttondelete = findViewById<View>(R.id.btndeleteprev) as Button
        buttonconfirm = findViewById<View>(R.id.btnconfirmpayment) as Button
        val quote = intent.getSerializableExtra("Quote") as? Quote
        if (quote!=null){
            Log.e("Quote", quote.toString())
            loadViews(quote)
        }

        val user = FirebaseAuth.getInstance().currentUser
        user?.let {
            val email = user.email.toString()
            val docRef = db().collection("utenti").document(email)
            docRef.get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        val ris = document.data?.get("tipo").toString()
                        Log.d(ContentValues.TAG, "DocumentSnapshot data: $ris")
                        if (ris == "2"){
                            if(quote?.status!="da accettare")
                            {
                                btndeleteprev.visibility = View.GONE
                                btnacceptprev.visibility = View.GONE
                            }
                            btnconfirmpayment.visibility = View.GONE
                        }
                        else if (ris == "3")
                        {
                            btndeleteprev.visibility = View.GONE
                            btnacceptprev.visibility = View.GONE
                            btnconfirmpayment.visibility = View.GONE
                        }else
                        {
                            if(quote?.status!="da pagare" && quote?.method !="sulposto")
                            {
                                btnconfirmpayment.visibility = View.GONE
                            }
                            btndeleteprev.visibility = View.GONE
                            btnacceptprev.visibility = View.GONE

                        }
                    } else {
                        Log.d(ContentValues.TAG, "No such document")
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d(ContentValues.TAG, "get failed with ", exception)
                }
        }


        buttonaccept!!.setOnClickListener {

            val db = FirebaseFirestore.getInstance()


            var docRef = db.collection("eventi").document(quote!!.event)
            docRef.get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        val intent = Intent(this@QuoteActivity, AcceptquoteActivity::class.java)



                        var evento = Event(
                            document.data!!["nome"].toString(),
                            document.data!!["data"].toString(),
                            document.data!!["orario"].toString(),
                            document.data!!["budget"].toString().toLong(),
                            document.data!!["luogo"].toString(),
                            document.data!!["proprietario"].toString(),
                            document.data!!["categoria"].toString(),
                            document.data!!["budgetattuale"].toString().toLong(),
                            arrayListOf(document.data!!["servizi"].toString()),
                            document.data!!["codiceinvito"].toString(),
                            arrayListOf(document.data!!["invitati"].toString()),
                            arrayListOf(document.data!!["tipiservizio"].toString())
                        )
                        Log.d(TAG, "leggo ${evento.name}")
                        intent.putExtra("Event", evento)
                        docRef = db.collection("servizi").document(quote.serv)
                        docRef.get()
                            .addOnSuccessListener { document ->
                                if (document != null) {
                                    var service = Service(
                                        document.data!!["nome"].toString(),
                                        document.data!!["descrizione"].toString(),
                                        document.data!!["immagine"].toString(),
                                        document.data!!["prezzo"].toString().toLong(),
                                        document.data!!["categoria"].toString(),
                                        document.data!!["orario"].toString(),
                                        document.data!!["proprietario"].toString(),
                                        document.data!!["luogo"].toString(),
                                        arrayListOf(document.data!!["giorni"].toString())
                                    )
                                    Log.d(TAG, "leggo ${service.name}")
                                    intent.putExtra("Service", service)

                                    intent.putExtra("Quote", quote)

                                    startActivity(intent)
                                } else {
                                    Log.d(TAG, "No such document")
                                }
                            }
                            .addOnFailureListener { exception ->
                                Log.d(TAG, "get failed with ", exception)
                            }


                    } else {
                        Log.d(TAG, "No such document")
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d(TAG, "get failed with ", exception)
                }

        }

        buttondelete!!.setOnClickListener {
            db().collection("preventivi").document(quote!!.code)
                .update("stato", "rifiutato")
                .addOnSuccessListener { Log.d(ContentValues.TAG, "DocumentSnapshot successfully written!") }
                .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }
            Toast.makeText(this, "Appuntamento annulato!", Toast.LENGTH_SHORT).show()
            startIntent(QuotelistActivity::class.java)
        }

        buttonconfirm!!.setOnClickListener {
            db().collection("preventivi").document(quote!!.code)
                .update("conferma", "pagato")
                .addOnSuccessListener { Log.d(ContentValues.TAG, "DocumentSnapshot successfully written!") }
                .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }
            Toast.makeText(this, "Appuntamento annullato!", Toast.LENGTH_SHORT).show()
            startIntent(QuotelistActivity::class.java)
            db().collection("preventivi").document(quote.code)
                .update("stato", "accettato")
                .addOnSuccessListener { Log.d(ContentValues.TAG, "Stato aggiornato!") }
                .addOnFailureListener { e -> Log.w(ContentValues.TAG, "Error writing document", e) }
        }

    }

    private fun loadViews(quote: Quote) {

        servprev.text =   "Servizio "+quote.serv
        fornitoreprev.text = "Fornitore: "+ quote.supplier
        organizzatoreprev.text = "Organizzatore: "+quote.manager
        eventprev.text = "Evento: "+ quote.event
        statusprev.text = "Stato: "+quote.status
        modalitaprev.text = "Modalità: "+quote.modality
        metodoprev.text = "Metodo: "+quote.method
        confprev.text = "Conferma: "+quote.confirm
    }

    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }

    fun db(): FirebaseFirestore {
        return FirebaseFirestore.getInstance()
    }

}