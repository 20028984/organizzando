package it.uniupo.organizzando.Model

import it.uniupo.organizzando.Event

class Post {
    private var postid:String=""
    private var postimage:String=""
    private var publisher:String=""
    private var caption:String=""
    private var event:String=""

    constructor()

    constructor(postid: String, postimage: String, publisher: String, caption: String, event: String) {
        this.postid = postid
        this.postimage = postimage
        this.publisher = publisher
        this.caption = caption
        this.event = event
    }

    //getters
    fun getPostId():String{
        return postid
    }

    fun getEvent():String{
        return event
    }

    fun getPostImage():String{
        return postimage
    }
    fun getPublisher():String{
        return publisher
    }
    fun getCaption():String{
        return caption
    }

    //setters
    fun setPostId(postid: String)
    {
        this.postid=postid
    }

    fun setPostImage(postimage: String)
    {
        this.postimage=postimage
    }

    fun setPublisher(publisher: String)
    {
        this.publisher=publisher
    }

    fun setCaption(caption: String)
    {
        this.caption=caption
    }

    fun setEvent(event: String)
    {
        this.event=event
    }
}