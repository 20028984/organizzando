package it.uniupo.organizzando


import android.app.Activity
import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import it.uniupo.organizzando.fragment.DatePickerFragment2
import it.uniupo.organizzando.fragment.TimePickerFragment4
import kotlinx.android.synthetic.main.activity_addevent.*

class AddeventActivity : AppCompatActivity() {
    private var buttonsubmit: Button? = null
    private var orariotext: TextView? = null
    private var datatext: TextView? = null
    private var luogotext: TextView? = null
    private val STRING_LENGTH = 10;
    private var btnback: ImageView? = null
    private val ALPHANUMERIC_REGEX = "[a-zA-Z0-9]+";
    private val charPool : List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addevent)

        buttonsubmit = findViewById<View>(R.id.btn_submit2) as Button
        orariotext = findViewById<View>(R.id.oraev) as TextView
        datatext = findViewById<View>(R.id.dataev) as TextView
        luogotext = findViewById<View>(R.id.luogoev) as TextView
        btnback = findViewById<View>(R.id.iconaback6) as ImageView

        orariotext!!.setOnClickListener {
            TimePickerFragment4().show(supportFragmentManager, "timePicker")
        }

        datatext!!.setOnClickListener {
            DatePickerFragment2().show(supportFragmentManager, "timePicker")
        }

        luogotext!!.setOnClickListener {
            val intent = Intent(this@AddeventActivity, MapsActivity::class.java)
            startActivityForResult(intent,1)
        }

        btnback!!.setOnClickListener(View.OnClickListener {
            startIntent(OrganizzatoremainActivity::class.java)
        })


        buttonsubmit!!.setOnClickListener {

            var cont=0
            val tipi = ArrayList<String>()

            if(fioraio.isChecked)
            {
                tipi.add("Fioraio")
                cont++
            }
            if(catering.isChecked)
            {
                tipi.add("Catering")
                cont++
            }
            if(parrucchiere.isChecked)
            {
                tipi.add("Parrucchiere")
                cont++
            }
            if(trasporto.isChecked)
            {
                tipi.add("Trasporto")
                cont++
            }
            if(security.isChecked)
            {
                tipi.add("Security")
                cont++
            }
            if(musica.isChecked)
            {
                tipi.add("Musica")
                cont++
            }
            if(fotografia.isChecked)
            {
                tipi.add("Fotografia")
                cont++
            }
            if(altro.isChecked)
            {
                tipi.add("Altro")
                cont++
            }

            if(cont==0){
                Toast.makeText(this, "Devi selezionare almeno una categoria!", Toast.LENGTH_SHORT).show()
            }else if (nomeev.text.isEmpty()) {
                Toast.makeText(this, "Devi inserire il nome!", Toast.LENGTH_SHORT).show()
            } else if (dataev.text == "Tocca per inserire la data") {
                Toast.makeText(this, "Devi inserire la data!", Toast.LENGTH_SHORT).show()
            } else if (luogoev.text  == "Tocca per il luogo in cui si svolgerà l\'evento") {
                Toast.makeText(this, "Devi inserire il luogo!", Toast.LENGTH_SHORT).show()
            } else if (budget.text.isEmpty()) {
                Toast.makeText(this, "Devi inserire il budget!", Toast.LENGTH_SHORT).show()
            } else if (oraev.text == "Tocca per inserire l\'ora") {
                Toast.makeText(this, "Devi inserire l'ora!", Toast.LENGTH_SHORT).show()
            } else {
                val randomString = (1..STRING_LENGTH)
                    .map { i -> kotlin.random.Random.nextInt(0, charPool.size) }
                    .map(charPool::get)
                    .joinToString("");

                val user2 = FirebaseAuth.getInstance().currentUser?.email
                val vuoto = ArrayList<String>()
                val db = FirebaseFirestore.getInstance()
                val evento = hashMapOf(
                    "nome" to nomeev.text.toString(),
                    "data" to dataev.text.toString(),
                    "luogo" to luogoev.text.toString(),
                    "budget" to budget.text.toString().toLong(),
                    "orario" to oraev.text.toString(),
                    "proprietario" to user2.toString(),
                    "categoria" to spinnercategorieevento.selectedItem.toString(),
                    "budgetattuale" to budget.text.toString().toLong(),
                    "servizi" to vuoto,
                    "codiceinvito" to randomString,
                    "invitati" to vuoto,
                    "tipiservizio" to tipi
                )

                db.collection("eventi").document("${nomeev.text}")
                    .set(evento)
                    .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully written!") }
                    .addOnFailureListener { e -> Log.w(TAG, "Error writing document", e) }

                val p = Event(
                    nomeev.text.toString(),
                    dataev.text.toString(),
                    oraev.text.toString(),
                    budget.text.toString().toLong(),
                    luogoev.text.toString(),
                    user2.toString(),
                    spinnercategorieevento.selectedItem.toString(),
                    budget.text.toString().toLong(),
                    vuoto,
                    randomString,
                    vuoto,
                    tipi
                )


                addEvent(p)

                startIntent(OrganizzatoremainActivity::class.java)
            }

        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            val returnString = data!!.getStringExtra("result")
            luogoev.text = returnString
        }
    }


    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }

    override fun onBackPressed() {
        startActivity(Intent(this, OrganizzatoremainActivity::class.java))
    }
}