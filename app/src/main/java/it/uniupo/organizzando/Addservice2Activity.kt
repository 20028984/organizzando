package it.uniupo.organizzando

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import it.uniupo.organizzando.fragment.TimePickerFragment1
import it.uniupo.organizzando.fragment.TimePickerFragment2
import kotlinx.android.synthetic.main.activity_addservice2.*
import java.io.ByteArrayOutputStream

class Addservice2Activity : AppCompatActivity() {
    private val PERMISSIONCODE = 1000;
    private val IMAGE_CAPTURE_CODE = 1001
    var image_uri: Uri? = null
    private var buttonsubmit: Button? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addservice2)
        val service = intent.getSerializableExtra("servizio") as? Service

        buttonsubmit = findViewById<View>(R.id.btn_submit2) as Button



        buttonsubmit!!.setOnClickListener{
            val giorni = ArrayList<String>()
            var cont=0

            if(Lunedi.isChecked)
            {
                giorni.add("Monday")
                cont++
            }
            if(Martedi.isChecked)
            {
                giorni.add("Tuesday")
                cont++
            }
            if(Mercoledi.isChecked)
            {
                giorni.add("Wednesday")
                cont++
            }
            if(Giovedi.isChecked)
            {
                giorni.add("Thursday")
                cont++
            }
            if(Venerdi.isChecked)
            {
                giorni.add("Friday")
                cont++
            }
            if(Sabato.isChecked)
            {
                giorni.add("Saturday")
                cont++
            }
            if(Domenica.isChecked)
            {
                giorni.add("Sunday")
                cont++
            }

            service!!.days=giorni

            if(cont>0)
            {
                val intent = Intent(this@Addservice2Activity, MapsaddservActivity::class.java)
                intent.putExtra("servizio", service)
                startActivity(intent)
            }else
            {
                Toast.makeText(this, "Devi inserire almeno un giorno!", Toast.LENGTH_SHORT).show()
            }



        }


    }


    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        image_uri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        //called when user presses ALLOW or DENY from Permission Request Popup
        when (requestCode) {
            PERMISSIONCODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED
                ) {
                    //permission from popup was granted
                    openCamera()
                } else {
                    //permission from popup was denied
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }


    fun db(): FirebaseFirestore {
        return FirebaseFirestore.getInstance()
    }

    private fun startIntent (theIntent: Class<*>) {

        val i = Intent(this, theIntent)
        startActivity(i)
    }

    override fun onBackPressed() {
        startActivity(Intent(this, FornitoremainActivity::class.java))
    }

}